/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import lapr.project.controller.*;
import java.util.ArrayList;
import javax.swing.JOptionPane;

import static com.oracle.nio.BufferSecrets.instance;
import java.util.ArrayList;
import static jdk.nashorn.internal.objects.Global.instance;
import lapr.project.model.CentroExposicoes;
import lapr.project.model.Exposicao;
import lapr.project.model.ExposicaoEstadoFaeSemDemonstracao;
import lapr.project.model.FAE;
import lapr.project.model.ListaFAE;
import lapr.project.model.Utilizador;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import static sun.awt.image.PixelConverter.ArgbPre.instance;
import static sun.font.SunLayoutEngine.instance;
import sun.security.jca.GetInstance.Instance;


/**
 *
 * @author Jorge
 */

public class DefinirFAEControllerTest {
    
    @Test
    public void EnsureRegistaMembroFAEWorks() {
        CentroExposicoes ce = new CentroExposicoes();
        FAE fae = new FAE();

        
        DefinirFAEController controller = new DefinirFAEController(ce);
        
        assertEquals(controller.registaMembroFAE(fae),true);
        
    }
    
    @Test
    public void addFAE() {
        ListaFAE lf = new ListaFAE();
        Exposicao e = new Exposicao();
        Utilizador u = new Utilizador();
        CentroExposicoes ce = new CentroExposicoes();
        
        FAE fae = new FAE();
        fae.setUtilizador(u);
        DefinirFAEController controller = new DefinirFAEController(ce);
        
        e.setListaFAE(lf);
        controller.addFAE(u, e);
        assertEquals(fae.getUtilizador().getNome(),controller.addFAE(u, e).getUtilizador().getNome());
        
    }

    @Test
    public void atualizaExposicao() {
       
       Exposicao e = new Exposicao();
       e.setEstado(new ExposicaoEstadoFaeSemDemonstracao(e));
        
       assertEquals(e.setFaeDefinido(),true);
       
    }
}
