/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import org.junit.Test;

/**
 *
 * @author lapr grupo 3
 */
public class RegistoStandTest {
    @Test
	public void ensureNovoStandWorks() {
            RegistoStand rc=new RegistoStand();
            assertNotEquals(rc.novoStand("ola", 22),null);
        }
    
        @Test
	public void ensureRegistoStandWorks() {
            RegistoStand rc=new RegistoStand();
            Stand s=new Stand();
            rc.RegistoStand(s);
            assertEquals(rc.getListaStands().contains(s),true);
        }
}
