/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.util.ArrayList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import org.junit.Test;

/**
 *
 * @author lapr grupo 3
 */
public class ListaAtribuicoesTest {
     @Test
	public void ensureNewAtribuicaoWorks() {
            ListaAtribuicoes la= new ListaAtribuicoes();
            Candidatura c=new Candidatura("aa","aa",22,22,"aa");
            FAE fae = new FAE();
            
            
            Atribuicao a=la.newAtribuicao(c, fae);
            assertNotEquals(a,null);
        }
    @Test
	public void ensureRegistarAtribuicoesWorks() {
            ListaAtribuicoes la= new ListaAtribuicoes();
            ArrayList<Atribuicao>laArray=new ArrayList();
            Candidatura c=new Candidatura("aa","aa",22,22,"aa");
            FAE fae = new FAE();
            Atribuicao a=new Atribuicao(fae,c);
            laArray.add(a);
            la.registarAtribuicoes(laArray);
            assertEquals(la.getListaAtribuicoes(),laArray);
        }
}
