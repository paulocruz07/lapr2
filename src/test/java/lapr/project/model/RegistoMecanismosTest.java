/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import org.junit.Test;

/**
 *
 * @author lapr grupo 3
 */
public class RegistoMecanismosTest {
     @Test
	public void ensureAddMecanismoWorks() {
            String s= "mecanismo a";
            RegistoMecanismos rm = new RegistoMecanismos();
            RegistoMecanismos.addMecanismo(s);
            
            
            assertEquals(rm.getListaMecanismos().get(0),s);
        }
       
}
