/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import org.junit.Test;

/**
 *
 * @author lapr grupo 3
 */
public class RegistoUtilizadoresTest {
    @Test
	public void ensureNovoUtilizadorWorks() {
          RegistoUtilizadores ru=new RegistoUtilizadores();
          Utilizador u=ru.novoUtilizador();
          assertNotEquals(u,null);
        }
        @Test
	public void ensureRegistaDadosWorks() {
            Utilizador u=new Utilizador();
            RegistoUtilizadores ru=new RegistoUtilizadores();
            ru.registaDados(u);
            assertEquals(ru.getListaUtilizadores().contains(u),true);
        }
         @Test
	public void ensureValidaUtilizadorWorks() {
             Utilizador u=new Utilizador();
            RegistoUtilizadores ru=new RegistoUtilizadores();
            u.setUsername("ola");
            ru.addUtilizador(u);
            boolean result=ru.validaUtilizador(u);
            assertEquals(false,result);
            
        }
        @Test
	public void ensureAddUtilizadorWorks() {
            Utilizador u=new Utilizador();
            RegistoUtilizadores ru=new RegistoUtilizadores();
            ru.addUtilizador(u);
            assertEquals(ru.getListaUtilizadores().contains(u),true);
        }
        
        
       
}
