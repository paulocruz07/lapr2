/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author lapr grupo 3
 */
public class ListaAtribuicaoStandsTest {
    
     @Test
	public void ensureNovaAtribuicaoWorks() {
            ListaAtribuicaoStands las=new ListaAtribuicaoStands();
            Exposicao e = new Exposicao();
            Stand s = new Stand();
            
            las.novaAtribuicao(e,s);
            
            assertEquals(las.getListaAtribuicoesStand().get(0).getExposicao(),e);
        }
    
}
