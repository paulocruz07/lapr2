/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.util.ArrayList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import org.junit.Test;

/**
 *
 * @author lapr grupo 3
 */
public class RegistoExposicoesTest {
    @Test
	public void ensureNovaExposicaoWorks() {
            RegistoExposicoes re = new RegistoExposicoes();
            Exposicao e = re.novaExposicao();
            assertNotEquals(e,null);
        }
        @Test
	public void ensureValidaExposicao1Works() {
            Exposicao e=new Exposicao();
            RegistoExposicoes re = new RegistoExposicoes();
            boolean result = re.validaExposicao1(e);
            boolean expected=true;
            assertEquals(expected,result);
        }
        
         @Test
	public void ensureRegistaExposicaoWorks() {
            RegistoExposicoes re = new RegistoExposicoes();
            Exposicao e=new Exposicao();
            boolean result=re.registaExposicao(e);
            assertEquals(true,result);
        }
        @Test
	public void ensureRegistaExposicaoFails() {
            RegistoExposicoes re = new RegistoExposicoes();
            Exposicao e=new Exposicao();
            re.registaExposicao(e);
            boolean result=re.registaExposicao(e);
            assertEquals(false,result);
        }
        
        @Test
	public void ensureGetListaExposicoesDoFAEWorks() {
            ArrayList<Exposicao> leExpected=new ArrayList();
            FAE fae=new FAE();
            ListaFAE lFae=new ListaFAE();
            lFae.addFAE(fae);
            Exposicao e = new Exposicao();
            e.setListaFAE(lFae);
            RegistoExposicoes re=new RegistoExposicoes();
            re.addExposicao(e);
            leExpected.add(e);
            assertEquals(re.getListaExposicoesDoFAE(fae),leExpected);
        }
         @Test
	public void ensureGetListaExposicoesDoOrganizadorWorks() {
            ArrayList<Exposicao> leExpected=new ArrayList();
            Organizador org=new Organizador();
            ListaOrganizadores lOrg=new ListaOrganizadores();
            lOrg.addOrganizador(org);
            Exposicao e = new Exposicao();
            e.setlistaOrganizadores(lOrg);
            RegistoExposicoes re=new RegistoExposicoes();
            re.addExposicao(e);
            leExpected.add(e);
            assertEquals(re.getListaExposicoesDoOrganizador(org),leExpected);
        }
         @Test
	public void ensureGetListaExposicoesDoOrganizadorNaoAtribuidasWorks() {
            ArrayList<Exposicao> leExpected=new ArrayList();
            Organizador org=new Organizador();
            ListaOrganizadores lOrg=new ListaOrganizadores();
            lOrg.addOrganizador(org);
            Exposicao e = new Exposicao();
            e.setlistaOrganizadores(lOrg);
            e.setEstado(new ExposicaoEstadoInicial(e));
            RegistoExposicoes re=new RegistoExposicoes();
            re.addExposicao(e);
            leExpected.add(e);
            assertEquals(re.getListaExposicoesDoOrganizadorNaoAtribuidas(org),leExpected);
        }
         @Test
	public void ensureGetExposicoesSemFAEWorks() {
            ArrayList<Exposicao> leExpected=new ArrayList();
            Organizador org=new Organizador();
            ListaOrganizadores lOrg=new ListaOrganizadores();
            lOrg.addOrganizador(org);
            Exposicao e = new Exposicao();
            e.setlistaOrganizadores(lOrg);
            e.setEstado(new ExposicaoEstadoDemonstracaoSemFAEDefinido(e));
            RegistoExposicoes re=new RegistoExposicoes();
            re.addExposicao(e);
            leExpected.add(e);
            assertEquals(re.getExposicoesSemFAE(org),leExpected);
        }
        @Test
	public void ensureAddExposicaoWorks() {
            RegistoExposicoes re = new RegistoExposicoes();
            Exposicao e=new Exposicao();
            re.addExposicao(e);
            boolean result=re.getListaExposicoes().contains(e);
            assertEquals(true,result);
        }
}
