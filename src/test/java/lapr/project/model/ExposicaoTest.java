/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import static com.oracle.nio.BufferSecrets.instance;
import java.util.ArrayList;
import static jdk.nashorn.internal.objects.Global.instance;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import static sun.awt.image.PixelConverter.ArgbPre.instance;
import static sun.font.SunLayoutEngine.instance;
import sun.security.jca.GetInstance.Instance;

/**
 *
 * @author lapr grupo 3
 */
public class ExposicaoTest {
    
     @Test
	public void ensureGetRegistoAtribuicaoWorks() {
                ListaAtribuicoes expected=new ListaAtribuicoes();
                ListaAtribuicoes result=new ListaAtribuicoes();
                
                Exposicao e = new Exposicao("titulo","descricao","ssd","sdsd,","sdds","ss","sdssd","asdasa");
                e.setListaAtribuicoes(expected);
                result=e.getRegistoAtribuicao();
                assertEquals(expected, result);
        }
    
        
         @Test
	public void ensureGetFAEWorks() {
            FAE expected= new FAE();
            FAE result=new FAE();
            
            ListaFAE listaFAE = new ListaFAE();
            listaFAE.addFAE(expected);
            result = listaFAE.getFAE(expected);
            assertEquals(expected, result);
        }
       
         @Test
	public void ensureGetOrganizadorWorks() {
            Utilizador u=new Utilizador();
            Organizador expected= new Organizador(u);
            Organizador result= new Organizador(u);
            ListaOrganizadores lo = new ListaOrganizadores();
            lo.addOrganizador(expected);
            result=lo.getOrganizador(expected);
            assertEquals(expected, result);
        }
         
       
        
        @Test
        public void ensureAdicionaCandidaturaWorks() {
           ListaCandidaturas lcExpected=new ListaCandidaturas();
           ArrayList<Candidatura> lcExpected2=new ArrayList();
           
           Candidatura c = new Candidatura("ad","ss",2,2,"ss");
           lcExpected2.add(c);
           lcExpected.setListaCandidatura(lcExpected2);
           Exposicao e = new Exposicao();
           e.adicionaCandidatura(c);
           assertEquals(lcExpected.getListaCandidatura(), e.getListaCandidatura().getListaCandidatura());
        }
          @Test
        public void ensureAdicionaStandWorks() {
           RegistoStand lsExpected=new RegistoStand();
           ArrayList<Stand> lsExpected2=new ArrayList();
           Stand s=new Stand();
           lsExpected2.add(s);
           lsExpected.setListaStands(lsExpected2);
           Exposicao e = new Exposicao();
           e.adicionaStand(s);
           assertEquals(lsExpected.getListaStands(), e.getListaStands().getListaStands());
        }
         @Test
        public void ensureGetEstadoWorks() {
           boolean seExpected=true;
           boolean seResult;
            Exposicao e=new Exposicao();
            e.setEstado(new ExposicaoEstadoInicial(e));
            
            SuperExposicao se2 = e.getEstado();
            seResult=se2 instanceof ExposicaoEstadoInicial;
            assertEquals(seExpected,seResult);
        }
        @Test
        public void ensureSetFaeDefinidoWorks() {
            boolean seExpected=false;
           boolean seResult;
            Exposicao e= new Exposicao();
            SuperExposicao se=new ExposicaoEstadoInicial(e);
            seResult=se.setFaeDefinido();
            assertEquals(seExpected,seResult);
        }
    
        @Test
        public void ensureIsValidForCriadaWorks() {
            boolean expected = true;
            Exposicao e = new Exposicao();
            ExposicaoEstadoInicial expInicial=new ExposicaoEstadoInicial(e);
            boolean result = expInicial.isValidaForCriada();
            assertEquals(expected,result);
        }
        
        
    @Test
	public void ensureToStringWorks() {
		String expected="";
                String result="";
                Exposicao e = new Exposicao("titulo","descricao","ssd","sdsd,","sdds","ss","sdssd","asdasa");
                expected = "titulo,descricao";
                result = e.toString();
		assertEquals(expected, result);
	}
       
        
    }
        

