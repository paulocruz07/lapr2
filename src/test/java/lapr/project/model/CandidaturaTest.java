/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.util.ArrayList;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author lapr grupo 3
 */
public class CandidaturaTest {
    @Test
	public void ensureNovaAvaliacaoWorks() {
            Candidatura c=new Candidatura("ss","ss",22,22,"ss");
            FAE fae = new FAE();
            Avaliacao avExpected=new Avaliacao(c,fae);
            Avaliacao avResult=c.novaAvaliacao(c,fae);
            assertEquals(avExpected.getInfoCandidatura(),avResult.getInfoCandidatura());
        }
        
        @Test
	public void ensureAddProdutoWorks() {
            Candidatura c=new Candidatura("aa","ss",22,22,"ss");
            Produto pExpected = new Produto("nome");
            c.addProduto(pExpected);
            assertEquals(pExpected,c.getListaProdutos().get(0));
        }
        @Test
	public void ensureAddKeywordsWorks() {
            Candidatura c=new Candidatura("aa","ss",22,22,"ss");
            Keyword k=new Keyword();
            c.addKeywords(k);
            assertEquals(k,c.getListaKeywords().get(0));
        }
        @Test
	public void ensureToStringWorks() {
            String expected = "Nome aa, morada ss, telemovel 22, quantidade de convites 22, area pretendida ss";
            Candidatura c=new Candidatura("aa","ss",22,22,"ss");
            assertEquals(c.toString(),expected);
        }
        
      
    }
       
    
        
    

