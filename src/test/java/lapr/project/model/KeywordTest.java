/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import javafx.scene.Node;
import javax.lang.model.element.Element;
import javax.swing.text.Document;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import lapr.project.utils.XMLParser;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import org.junit.Test;

/**
 *
 * @author grupo 3 lapr
 */
public class KeywordTest {
    @Test
	public void ensureSameContentObjectsAreEqual() {
		Keyword expected = new Keyword("Doors");
		Keyword result = new Keyword("Doors");
		assertEquals(expected, result);
	}

	@Test
	public void ensureSameObjectIsEqual() {
		Keyword expected = new Keyword("Doors");
		assertEquals(expected, expected);
	}

	@Test
	public void ensureDifferentObjectsAreNotEqual() {
		Keyword expected = new Keyword("Doors");
		Object result = new Object();
		assertNotEquals(expected, result);
	}

	@Test
	public void ensureHashCodeIsCorrect() {
		Keyword firstKeywordExample = new Keyword("Doors");

		int expected = 66216549;
		int result = firstKeywordExample.hashCode();
		assertEquals(expected, result);
        }
        @Test
	public void ensureToCSVStringIsCorrect() {
            Keyword firstKeywordExample = new Keyword("Doors");
            String expected="Doors";
            assertEquals(expected,firstKeywordExample.toCSVString());
        }
	
}
