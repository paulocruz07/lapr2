/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import org.junit.Test;

/**
 *
 * @author lapr grupo 3
 */
public class RegistoRecursoTest {
    
    @Test
	public void ensureNovoRecursoWorks() {
            RegistoRecurso rc=new RegistoRecurso();
            assertNotEquals(rc.novoRecurso("ola"),null);
        }
     @Test
	public void ensureRegistaRecursoWorks() {
            RegistoRecurso rc=new RegistoRecurso();
            Recurso r = new Recurso();
            rc.registaRecurso(r);
            assertEquals(rc.getListaRecursos().contains(r),true);
        }
}
