/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.ui;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import lapr.project.controller.*;
import lapr.project.model.*;


/**
 *
 * @author 1150855_1150856
 */
public class DialogoUC7 extends JDialog{
    /**
     * variável de instância JComboBox fae, para apresentar os fae's registados no sistema.
     */
    private JComboBox utilizadorNaoRegistado;
    /**
     * variável de instância JButton btnMostrarCandidatura, para mostrar a candidatura selecionada.
     */
    private JButton btnMostrarUtilizadorNaoRegistado;
    /**
     * variável de instância Janela framePai, para referência de onde o JDialog foi lançado.
     */
    private Janela framePai;
    /**
     * variável de instância AvaliarCandidaturasController controllerUi, para comunicar com o controller.
     */
    private ConfirmarRegistoUtilizadorController controllerUi;
    /**
     * variável de classe FAE faeSelecionado, para reter o fae selecionado na JComboBox dos fae.
     */
    private Utilizador utilizadorSelecionado;
    
    private JList listaDecisoes;
      
    
    public DialogoUC7(Janela framePai,ConfirmarRegistoUtilizadorController controllerUi) {
     
        super(framePai,"Confirmar Registo Utilizador", true);
        
        this.framePai = framePai;
     
        setLayout(new GridLayout(3,1));
        this.controllerUi=controllerUi;
        
        criarComponentes();
        
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        pack();
        setResizable(false);
        setLocationRelativeTo(framePai);
        setVisible(true);
    }
    
   
    private void criarComponentes(){
        JPanel p1 = criarPainelComboBoxUtilizadorNaoRegistado();
        JPanel p2 = criarPainelMostrarUtilizadorNaoRegistado();
        JPanel p3 = criarPainelBotoes();
        
        add(p1);
        add(p2);
        add(p3);
        
    }

    /**
     * Método utilizado para criar o painel referente á JComboBox fae
     * @return - painel devidamente criado e tratado.
     */
     private JPanel criarPainelComboBoxUtilizadorNaoRegistado() {
        JLabel lbl1 = new JLabel("Utilizador Não Registado:", JLabel.RIGHT);
        
        
        utilizadorNaoRegistado = criarComboBoxUtilizadorNaoRegistado();
         
       
        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 3, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl1);
        p.add(utilizadorNaoRegistado);

        return p;
    }
     
   
    private JComboBox criarComboBoxUtilizadorNaoRegistado(){
        JComboBox cb = new JComboBox();
        cb.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                if(utilizadorNaoRegistado!=null){
                    utilizadorSelecionado=(Utilizador) utilizadorNaoRegistado.getSelectedItem();
                }
            }
        });
        
        ArrayList<Utilizador> ls = controllerUi.getUtilizadores();
        for(Utilizador a:ls){
            if(a.getRegistado()==false && a.getConfirmado()==false){
                cb.addItem(a);
            }
        }
        return cb;
    }
    

    private JPanel criarPainelMostrarUtilizadorNaoRegistado(){   
        btnMostrarUtilizadorNaoRegistado=criarBotaoMostrarUtilizadorNaoRegistado();
        
        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnMostrarUtilizadorNaoRegistado);
        return p;
    }
    
    /**
     * Método utilizado para criar o botao para mostrar a candidatura selecionada
     * @return - botao devidamente criado e tratado.
     */
    private JButton criarBotaoMostrarUtilizadorNaoRegistado(){
        JButton btn=new JButton("Mostrar Utilizador Não Registado");
        btn.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                if(utilizadorSelecionado!=null){
                    JOptionPane.showMessageDialog(null,controllerUi.getRegistoUtilizadores(utilizadorSelecionado));
                }else{
                    JOptionPane.showMessageDialog(rootPane,"Não há utilizadores a mostrar");
                }      
            }
        });
        return btn;
    }
    
   
    private JPanel criarPainelBotoes() {
        JButton btnAceitar = criarBotaoAceitar();
        getRootPane().setDefaultButton(btnAceitar);

        JButton btnCancelar = criarBotaoCancelar();
        JButton btnRejeitar= criarBotaoRejeitar();

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnAceitar);
        p.add(btnRejeitar);
        p.add(btnCancelar);
        

        return p;
    }
    /**
     * Método utilizado para criar o botao responsavel por guardar a decisao e a justificacao da candidatura aceite pelo fae
     * @return - botao devidamente criado e tratado.
     */
    private JButton criarBotaoAceitar() {        
        JButton btn = new JButton("Aceitar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                 if(utilizadorNaoRegistado.getItemCount()!=0){
                    controllerUi.confirmaRegisto(true);
                    dispose();
                    }
                    else{
                        JOptionPane.showMessageDialog(null, "Sem utilizadores!");
                    }            
                }

        });
        return btn;
    }

    /**
     * Método utilizado para criar o botao responsavel por cancelar as operações
     * @return 
     */
    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                utilizadorSelecionado=null;
                dispose();
            }
            
        });
        return btn;
    }
    /**
     * Método utilizado para criar o botao responsavel por guardar a decisao e a justificacao da candidatura rejeitada pelo fae
     * @return 
     */
   private JButton criarBotaoRejeitar(){
      JButton btn = new JButton("Rejeitar");
        btn.addActionListener(new ActionListener() {
            @Override
             public void actionPerformed(ActionEvent e) {
                 if(utilizadorNaoRegistado.getItemCount()!=0){
                    controllerUi.confirmaRegisto(false);
                    
                    dispose();
                    }
                    else{
                        JOptionPane.showMessageDialog(null, "Sem utilizadores!");
                    }            
                }

        });
        return btn;
   }
}
