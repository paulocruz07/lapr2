/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.ui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import lapr.project.controller.ConfirmarRegistoUtilizadorController;
import lapr.project.controller.DefinirFAEController;
import lapr.project.model.Exposicao;
import lapr.project.model.FAE;
import lapr.project.model.ListaOrganizadores;
import lapr.project.model.Organizador;
import lapr.project.model.Utilizador;

/**
 *
 * @author Jorge
 */
public class DialogoUC2 extends JDialog {

    private FAE faeNovo;
    /**
     * variável de instância JComboBox fae, para apresentar os fae's registados
     * no sistema.
     */
    private JComboBox exposicao;
    /**
     * variável de instância JComboBox fae, para apresentar os fae's registados
     * no sistema.
     */
    private JComboBox fae;
    /**
     * variável de instância JButton btnMostrarCandidatura, para mostrar a
     * candidatura selecionada.
     */
    private JButton btnMostrarUtilizadorNaoRegistado;
    /**
     * variável de instância Janela framePai, para referência de onde o JDialog
     * foi lançado.
     */
    private Janela framePai;
    /**
     * variável de instância AvaliarCandidaturasController controllerUi, para
     * comunicar com o controller.
     */
    private DefinirFAEController controllerUi;
    /**
     * variável de classe FAE faeSelecionado, para reter o fae selecionado na
     * JComboBox dos fae.
     */
    private JButton BtnOK;
    private Exposicao exposicaoSelecionada;
    private FAE faeSelecionado;

    private boolean flag = false;
    private ListaOrganizadores ListaOrganizadores = new ListaOrganizadores();
    private Organizador Organizador;

    public DialogoUC2(Janela framePai, DefinirFAEController controllerUi, Utilizador u) {

        super(framePai, "Definir FAE", true);

        this.framePai = framePai;
        ArrayList<Organizador> lo = ListaOrganizadores.getListaOrganizadores();
        for (Organizador o : lo) {
            if (u.getEmail().equalsIgnoreCase(o.getUtilizador().getEmail())) {
                Organizador = o;
            }
        }

        setLayout(new GridLayout(2, 2, 20, 10));
        this.controllerUi = controllerUi;

        criarComponentes(Organizador);

        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        pack();
        setResizable(false);
        setLocationRelativeTo(framePai);
        setVisible(true);
    }

    private void criarComponentes(Organizador u) {
        JPanel p1 = criarPainelComboBoxExposicoes(u);
        JPanel p2 = criarPainelComboBoxFAEs();
        JPanel p3 = criarPainelBotaoDecidireOK();
        JPanel p4 = criaPainelBotaoCancelar();

        add(p1);
        add(p2);
        add(p3);
        add(p4);

    }

    /**
     * Método utilizado para criar o painel referente á JComboBox fae
     *
     * @return - painel devidamente criado e tratado.
     */
    private JPanel criarPainelComboBoxExposicoes(Organizador u) {
        JLabel lbl1 = new JLabel("Selecionar Exposição:", JLabel.RIGHT);

        exposicao = criarComboBoxExposicoes(u);

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 3, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl1);
        p.add(exposicao);

        return p;
    }

    private JComboBox criarComboBoxExposicoes(Organizador u) {
        JComboBox cb = new JComboBox();
        cb.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (exposicao != null) {
                    exposicaoSelecionada = (Exposicao) exposicao.getSelectedItem();
                    controllerUi.setExposicao(exposicaoSelecionada);
                }

            }
        });
        ArrayList<Exposicao> le = controllerUi.getExposicoesOrganizador(u);
        for (Exposicao a : le) {
            cb.addItem(a);
        }
        return cb;
    }

    private JPanel criarPainelComboBoxFAEs() {
        JLabel lbl1 = new JLabel("Selecionar FAE:", JLabel.RIGHT);

        fae = criarComboBoxFAEs();

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 3, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl1);
        p.add(fae);

        return p;
    }

    /**
     * Método utilizado para criar o botao para mostrar a candidatura
     * selecionada
     *
     * @return - botao devidamente criado e tratado.
     */
    private JComboBox criarComboBoxFAEs() {
        JComboBox cb = new JComboBox();
        cb.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (fae != null) {
                    faeSelecionado = new FAE((Utilizador) fae.getSelectedItem());
                }
            }
        });

        ArrayList<Utilizador> lf = controllerUi.getListaUtilizadores();
        for (Utilizador a : lf) {
            if (a.getFuncao().equalsIgnoreCase("fae")) {
                cb.addItem(a);
            }
        }
        return cb;
    }

    private JPanel criarPainelBotaoDecidireOK() {

        JButton btnDecidir = criarBotaoDecidir();
        BtnOK = criarBotaoOK();
        BtnOK.setEnabled(flag);
        getRootPane().setDefaultButton(btnDecidir);

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(btnDecidir);
        p.add(BtnOK);

        return p;
    }

    private JPanel criaPainelBotaoCancelar() {
        JButton btnCancelar = criarBotaoCancelar();

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnCancelar);

        return p;
    }

    private JButton criarBotaoDecidir() {
        JButton btn = new JButton("Decidir");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (fae.getItemCount() != 0 && exposicao.getItemCount() != 0) {
                    faeNovo=controllerUi.addFAE(faeSelecionado.getUtilizador(), exposicaoSelecionada);
                    boolean verificacao=controllerUi.registaMembroFAE(faeNovo);
                    if(verificacao){
                        JOptionPane.showMessageDialog(null, "Registo bem sucedido");
                    }else{
                        JOptionPane.showMessageDialog(null, "Registo mal sucedido");
                    }
                    flag = true;
                    BtnOK.setEnabled(flag);
                } else {
                    JOptionPane.showMessageDialog(null, "Sem FAEs ou exposições");
                }
            }

        });
        return btn;
    }

    private JButton criarBotaoOK() {
        JButton BtnOK = new JButton("OK");
        BtnOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int dialogResult = JOptionPane.showConfirmDialog(null, "Confirmar definição de FAEs?", "Atenção", JOptionPane.YES_NO_OPTION);
                if (dialogResult == 0) {
                    controllerUi.atualizaExposicao();
                    dispose();
                }
            }
        });
        return BtnOK;
    }

    /**
     * Método utilizado para criar o botao responsavel por cancelar as operações
     *
     * @return
     */
    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                exposicaoSelecionada = null;
                faeSelecionado = null;
                flag = false;
                dispose();
            }

        });
        return btn;
    }
}
