/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.ui;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import lapr.project.model.*;
import lapr.project.controller.*;
/**
 *
 * @author 1150855_1150856
 */


 public class DialogoUC5 extends JDialog {
    
    /**
     * variável de instância JComboBox exposicao, para apresentar as exposicoes registadas no sistema.
     */
    private JComboBox exposicao;
    /**
     * variável de instância JTextField nome para ler o nome da empresa,morada para ler a morada da empresa,
     * telemovel para ler o telemovel da empresa, area para ler a area pretendida pela empresa,
     * qtConvites para ler a quantidade de convites pretendidos pela empresa,
     * produto para ler para uma lista de produtos, os produtos pretendidos pela empresa.
     */
    private JTextField nome, morada, telemovel, area, qtConvites, produto, keyword;
    /**
     * variável de instância JTextArea listaProdutos, para apresentar os produtos ja introduzidos.
     */
    private JTextArea listaProdutos;
    /**
     * variável de instância Janela framePai, para referência de onde o JDialog foi lançado.
     */
    private JTextArea listaKeywords;
    
    private Janela framePai;
    /**
     * variável de instância JButton btnOkProduto, para introduzir na lista de produtos o produto inserido.
     */
    private JButton btnOkProduto, btnOkKeyword;
    /**
     * variável de instância Exposicao exposicaoSelecionada, para reter a exposicao selecionada na comboBox das exposicoes
     */
    private Exposicao exposicaoSelecionada;
    /**
     * variável de instância RegistaCandidaturaController controllerUi, para comunicar com o controller
     */
    private RegistaCandidaturaController controllerUi;
    
    /**
     * Método utilizado para construir todo o JDialog DialogoUC5
     * @param framePai - frame do qual este JDialog foi invocado.
     * @param controllerUi - controller passado por parâmetro já inicializado.
     */
   
    public DialogoUC5(Janela framePai,RegistaCandidaturaController controllerUi) {
    
        super(framePai, "Criar candidatura", true);

        this.framePai = framePai;
    
        setLayout(new GridLayout(7,2,10,0));
        this.controllerUi=controllerUi;
        
        criarComponentes();
        
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        pack();
        setResizable(false);
        setLocationRelativeTo(framePai);
        setVisible(true);
    }
    /**
     * Método utilizado para criar todos os elementos do JDialog
     */
    
    private void criarComponentes(){
        JPanel p1 = criarPainelExposicao();
        JPanel p2 = criarPainelNomeEmpresa();
        JPanel p3 = criarPainelMorada();
        JPanel p4 = criarPainelTelemovel();
        JPanel p5 = criarPainelArea();
        JPanel p6 = criarPainelQtConvites();
        JPanel p7 = criarPainelProduto();
        JPanel p8 = criarPainelListaProdutos();
        JPanel p9 = criarPainelBotoes();
        JPanel p10= criarPainelKeywords();
        JPanel p11=criarPainelListaKeywords();
        
        add(p1);
        add(p2);
        add(p7);
        add(p10);
        add(p8);
        add(p11);
        add(p3);
        add(p4);
        add(p5);
        add(p6);
        add(p9);
  
    }

    /**
     * Método utilizado para criar o painel referente á JComboBox exposicao
     * @return - painel devidamente criado e tratado
     */
    private JPanel criarPainelExposicao(){
        JPanel p=new JPanel();
        JLabel lbl1 = new JLabel("Exposicao:", JLabel.RIGHT);
        exposicao = criarComboBoxExposicao();
        
        
        p.add(lbl1);
        p.add(exposicao);
        return p;
    }
    /**
     * Método utilizado para criar a comboBox exposicao
     * @return - comboBox devidamente criada e tratada.
     */
    private JComboBox criarComboBoxExposicao(){
        JComboBox cb=new JComboBox();
        cb.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                if(exposicao!=null){
                    exposicaoSelecionada=(Exposicao) exposicao.getSelectedItem();
                    controllerUi.setExposicao(exposicaoSelecionada);
                }
            }
        });
        
        ArrayList<Exposicao> array=controllerUi.getListaExposicoes();
        for(Exposicao e:array){
            cb.addItem(e);
        }
        return cb;
    }
    /**
     * Método utilizado para criar o painel referente á TextField nome
     * @return - painel devidamente criado e tratado.
     */
     private JPanel criarPainelNomeEmpresa() {
        JLabel lbl1 = new JLabel("Nome da empresa:", JLabel.RIGHT);
        nome = new JTextField(10);
        
        JPanel p = new JPanel();
        
        p.add(lbl1);
        p.add(nome);

        return p;
    }
     
    /**
     * Método utilizado para criar o painel referente á TextField morada
     * @return - painel devidamente criado e tratado.
     */
     private JPanel criarPainelMorada(){
        JLabel lbl1 = new JLabel("Morada:", JLabel.RIGHT); 
        
        morada = new JTextField(10);

        JPanel p = new JPanel();
       
        p.add(lbl1);
        p.add(morada);

        return p;
    }
    
     /**
      * Método utilizado para criar o painel referente á TextField telemovel
     * @return - painel devidamente criado e tratado.
      */
     
    private JPanel criarPainelTelemovel() {
        JLabel lbl1 = new JLabel("Telemóvel:", JLabel.RIGHT); 
       
        
        telemovel = new JTextField(10);
        
        JPanel p = new JPanel();
        

        p.add(lbl1);
        p.add(telemovel);

        return p;
    }
    
    /**
     * Método utilizado para criar o painel referente á TextField area
     * @return - painel devidamente criado e tratado.
     */
     private JPanel criarPainelArea(){
       JLabel lbl1 = new JLabel("Área:", JLabel.RIGHT); 
        
        area = new JTextField(10);
        
        JPanel p = new JPanel();
       
        p.add(lbl1);
        p.add(area);

        return p;
    }
   
     /**
      * Método utilizado para criar o painel referente á TextField qtConvites
     * @return - painel devidamente criado e tratado.
      */
     
    private JPanel criarPainelQtConvites() {
        JLabel lbl1 = new JLabel("Quantidade de Convites:", JLabel.RIGHT); 
        
        qtConvites = new JTextField(10);
        
        JPanel p = new JPanel();
      
        p.add(lbl1);
        p.add(qtConvites);

        return p;
    }
    /**
     * Método utilizado para criar o painel referente ao produto a inserir
     * @return - painel devidamente criado e tratado.
     */
    private JPanel criarPainelProduto(){
        JLabel lbl1 = new JLabel("Produto:", JLabel.RIGHT); 
        
        btnOkProduto = criarBotaoOK();
        produto = new JTextField(7);
       
        
        JPanel p = new JPanel();
       

        p.add(lbl1);
        p.add(produto);
        p.add(btnOkProduto);

        return p;
    }
    /**
     * Método utilizado para criar o botao okProduto
     * @return - botao devidamente criado e tratado
     */
    
     private JButton criarBotaoOK(){
         JButton btn1= new JButton("Ok");
         
         btn1.addActionListener(new ActionListener(){
             @Override
             public void actionPerformed(ActionEvent e){
                 int x=0;
                 if(!produto.getText().trim().equals("")){
                    String s = listaProdutos.getText();
                    String[] vetorProdutos=s.split("\n");
                    for(int i=0;i<vetorProdutos.length;i++){   
                        if(produto.getText().equals(vetorProdutos[i])){
                            JOptionPane.showMessageDialog(null, "Produto já existe na lista");
                            x++;
                            produto.setText("");
                        }
                    }
                    if(x==0){
                        listaProdutos.append(produto.getText().trim()+"\n");
                    }
                        produto.setText("");
                        produto.requestFocus();
                 
                        
                 }
               }
         });
         return btn1;
     }
     
     
     private JPanel criarPainelKeywords(){
        JLabel lbl1 = new JLabel("Keyword:", JLabel.RIGHT); 
        
        btnOkKeyword = criarBotaoOKKeyword();
        keyword = new JTextField(7);
       
        
        JPanel p = new JPanel();
       

        p.add(lbl1);
        p.add(keyword);
        p.add(btnOkKeyword);

        return p;
    }
    /**
     * Método utilizado para criar o botao okProduto
     * @return - botao devidamente criado e tratado
     */
    
     private JButton criarBotaoOKKeyword(){
         JButton btn1= new JButton("Ok");
         
         btn1.addActionListener(new ActionListener(){
             @Override
             public void actionPerformed(ActionEvent e){
                 int x=0;
                 if(!keyword.getText().trim().equals("")){
                    String s = listaKeywords.getText();
                    String[] vetorKeywords=s.split("\n");
                    for(int i=0;i<vetorKeywords.length;i++){   
                        if(keyword.getText().equals(vetorKeywords[i])){
                            JOptionPane.showMessageDialog(null, "Keyword já existe na lista");
                            x++;
                            keyword.setText("");
                        }
                    }
                    if(x==0){
                        listaKeywords.append(keyword.getText().trim()+"\n");
                    }
                        keyword.setText("");
                        keyword.requestFocus();
                    
                 }
               }
         });
         return btn1;
     }
     
      private JPanel criarPainelListaKeywords(){
        listaKeywords = new JTextArea(5, 20);
        JScrollPane scrollPane = new JScrollPane(listaKeywords); 
        listaKeywords.setEditable(false);
    
        JPanel p = new JPanel();
       
        p.add(scrollPane);
        
        return p;
    }
     
     
    /**
     * Método utilizado para criar o painel referente á TextArea listaProdutos
     * @return - painel devidamente criado e tratado.
     */
    
    private JPanel criarPainelListaProdutos(){
        listaProdutos = new JTextArea(5, 20);
        JScrollPane scrollPane = new JScrollPane(listaProdutos); 
        listaProdutos.setEditable(false);
    
        JPanel p = new JPanel();
      
        p.add(scrollPane);
        
        return p;
    }
    
    /**
     * Método utilizado para criar o painel referente aos botoes
     * @return - painel devidamente criado e tratado.
     */
    private JPanel criarPainelBotoes() {
        JButton btnConfirmar = criarBotaoConfirmar();
        getRootPane().setDefaultButton(btnConfirmar);

        JButton btnCancelar = criarBotaoCancelar();
        JPanel p = new JPanel();
        
        p.add(btnConfirmar);
        p.add(btnCancelar);

        return p;
    }

    
    /**
     * Método utilizado para criar o botao responsavel por guardar os dados introduzidos referentes á candidatura
     * na exposicao selecionada
     * @return - JButton devidamente criado e tratado.
     */
    private JButton criarBotaoConfirmar() {        
        JButton btn = new JButton("Confirmar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                     Candidatura c = new Candidatura(nome.getText(), morada.getText(),
                             Integer.parseInt(telemovel.getText()), Integer.parseInt(qtConvites.getText()), area.getText());
                     String s = listaProdutos.getText();
                     String[] vetorProdutos = s.split("\n");
                     String s2 = listaKeywords.getText();
                     String[] vetorKeywords = s2.split("\n");
                     if (vetorKeywords.length >= 2 && vetorKeywords.length <= 5) {
                         if (!(nome.getText().trim().equals(""))
                                 && !(morada.getText().trim().equals("")) && !(area.getText().trim().equals(""))
                                 && vetorProdutos.length > 0) {
                             if (Integer.parseInt(telemovel.getText()) > 99999999
                                     && Integer.parseInt(telemovel.getText()) < 1000000000) {
                                     if(Integer.parseInt(qtConvites.getText()) > 0){
                                 for (int i = 0; i < vetorProdutos.length; i++) {
                                     c.addProduto(new Produto(vetorProdutos[i]));
                                 }
                                 for(int i=0;i<vetorKeywords.length;i++){   
                                        c.addKeywords(new Keyword(vetorKeywords[i]));
                                 }
                                 controllerUi.getExposicao().adicionaCandidatura(c);
                                 dispose();
                                     }else{JOptionPane.showMessageDialog(null, "Quantidade de convites inválido");
                                     qtConvites.setText("");
                                     qtConvites.requestFocus();
                                     }
                             } else {
                                 JOptionPane.showMessageDialog(null, "Número de telemóvel inválido");
                                 telemovel.setText("");
                                 telemovel.requestFocus();
                             }
                         } else {
                             JOptionPane.showMessageDialog(null, "Parametros em falta, preencha os restantes");
                         }
                     } else {
                         JOptionPane.showMessageDialog(null, "Insira entre 2 a 5 keywords");
                         listaKeywords.setText("");
                     }
               }catch(NumberFormatException err)   {
                   JOptionPane.showMessageDialog(null,"O formato dos parâmetros do tipo inteiro não é válido. " + err.getMessage());
                   telemovel.setText("");
                   qtConvites.setText("");
               }catch(Exception er){
                JOptionPane.showMessageDialog(null,"Erro: " + er.getMessage());
                }
            } 
        });
        
        return btn;
    }
    /**
     * Método utilizado para criar o painel referente ao botao responsavel por cancelar as operacoes
     * @return - JButton devidamente criado e tratado.
     */
    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btn;
    }

}
