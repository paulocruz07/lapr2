/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.ui;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import lapr.project.model.*;
import lapr.project.controller.*;

/**
 *
 * @author 1150855_1150856
 */
public class DialogoUC4 extends JDialog {

    /**
     * variável de instância JComboBox exposicoesDoFAE, para apresentar as
     * exposicoes do FAE.
     */
    private JComboBox exposicoesDoFAE;
    /**
     * variável de instância JComboBox fae, para apresentar os fae's registados
     * no sistema.
     */
    private JComboBox fae;
    /**
     * variável de instância JComboBox candidaturas, para apresentar as
     * candidaturas associadas ao fae selecionado.
     */
    private JComboBox candidaturas;
    /**
     * variável de instância JButton btnMostrarCandidatura, para mostrar a
     * candidatura selecionada.
     */
    private JButton btnMostrarCandidatura;
    /**
     * variável de instância JTextField justificacao, para o fae apresentar a
     * justificação da avaliação.
     */
    private JTextField justificacao;
    private JComboBox conhecimentoFAE;
    private JComboBox adequacaoCandidaturaExposicao;
    private JComboBox adequacaoCandidaturaDemonstracoes;
    private JComboBox adequacaoNrConvites;
    private JComboBox recomendacaoGlobal;
    
    
    /**
     * variável de instância Janela framePai, para referência de onde o JDialog
     * foi lançado.
     */
    private Janela framePai;
    /**
     * variável de instância AvaliarCandidaturasController controllerUi, para
     * comunicar com o controller.
     */
    private AvaliarCandidaturasController controllerUi;
    /**
     * variável de classe FAE faeSelecionado, para reter o fae selecionado na
     * JComboBox dos fae.
     */
    private FAE faeSelecionado;
    /**
     * variável de classe Candidatura candSelecionada, para reter a candidatura
     * selecionada na JComboBox das candidaturas.
     */
    private Candidatura candSelecionada;

    /**
     * Método utilizado para construir todo o JDialog DialogoUC4
     *
     * @param framePai - frame do qual este JDialog foi invocado.
     * @param controllerUi - controller passado por parâmetro já inicializado.
     */
    public DialogoUC4(Janela framePai, AvaliarCandidaturasController controllerUi) {

        super(framePai, "Avaliar candidatura", true);

        this.framePai = framePai;

        setLayout(new GridLayout(11, 1));
        this.controllerUi = controllerUi;

        criarComponentes();

        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        pack();
        setResizable(false);
        setLocationRelativeTo(framePai);
        setVisible(true);
    }

    /**
     * Método utilizado para criar todos os elementos do JDialog
     */
    private void criarComponentes() {
        JPanel p1 = criarPainelComboBoxFAE();
        JPanel p2 = criarPainelComboBoxExposicao();
        JPanel p3 = criarPainelComboBoxCandidaturas();
        JPanel p4 = criarPainelMostrarCandidatura();
        JPanel p5 = criarPainelBotoes();
        JPanel p6 = criarPainelJustificacao();
        JPanel p7 = criarPainelConhecimentoFAE();
        JPanel p8=criarPainelAdequacaoCandidaturaExposicao();
        JPanel p9=criarPainelAdequacaoCandidaturaDemonstracoes();
        JPanel p10=criarPainelAdequacaoNrConvites();
        JPanel p11=criarPainelRecomendacaoGlobal();
        
        add(p1);
        add(p2);
        add(p3);
        add(p4);
        add(p5);
        add(p6);
        add(p7);
        add(p8);
        add(p9);
        add(p10);
        add(p11);
    }

    /**
     * Método utilizado para criar o painel referente á JComboBox fae
     *
     * @return - painel devidamente criado e tratado.
     */
    private JPanel criarPainelComboBoxFAE() {
        JLabel lbl1 = new JLabel("FAE:", JLabel.RIGHT);

        fae = criarComboBoxFAE();

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 3, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl1);
        p.add(fae);

        return p;
    }

    /**
     * Método utilizado para criar a JComboBox fae
     *
     * @return - comboBox devidamente criada e tratada
     */
    private JComboBox criarComboBoxFAE() {
        JComboBox cb = new JComboBox();
        cb.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (fae != null) {
                    faeSelecionado = (FAE) fae.getSelectedItem();
                    if (exposicoesDoFAE != null) {
                        preencherComboBoxExposicao();
                    }
                }
            }
        });

        ArrayList<FAE> ls = controllerUi.getListaFAE();
        for (FAE a : ls) {
            cb.addItem(a);
        }
        return cb;
    }

    /**
     * Método utilizado para criar o painel referente á JComboBox
     * exposicoesDoFAE
     *
     * @return - painel devidamente criado e tratado
     */
    private JPanel criarPainelComboBoxExposicao() {
        JLabel lbl1 = new JLabel("Exposições do FAE:", JLabel.RIGHT);

        exposicoesDoFAE = criarComboBoxExposicoesDoFAE();
        preencherComboBoxExposicao();

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 3, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl1);
        p.add(exposicoesDoFAE);

        return p;
    }

    /**
     * Método utilizado para criar a comboBox exposicoesDoFAE
     *
     * @return - comboBox devidamente criada e tratada
     */
    private JComboBox criarComboBoxExposicoesDoFAE() {
        JComboBox cb = new JComboBox();
        cb.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (exposicoesDoFAE != null) {
                    controllerUi.setExposicao((Exposicao) exposicoesDoFAE.getSelectedItem());
                    preencherComboBoxCandidaturas();
                }
            }
        });
        return cb;
    }

    /**
     * Método utilizado para preencher a comboBox exposicoesDoFAE com a
     * informação correta.
     */
    private void preencherComboBoxExposicao() {
        if (faeSelecionado != null) {
            exposicoesDoFAE.removeAllItems();
            ArrayList<Exposicao> arrayExpo = controllerUi.getListaExposicoesDoFAE(faeSelecionado);
            for (Exposicao a : arrayExpo) {
                exposicoesDoFAE.addItem(a);
            }
        }
    }

    /**
     * Método utilizado para criar o painel referente á comboBox candidaturas
     *
     * @return - painel devidamente criado e tratado
     */
    private JPanel criarPainelComboBoxCandidaturas() {
        JLabel lbl1 = new JLabel("Candidaturas:", JLabel.RIGHT);

        candidaturas = criarComboBoxCandidaturas();

        preencherComboBoxCandidaturas();
        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl1);
        p.add(candidaturas);

        return p;
    }

    /**
     * Método utilizado para criar a comboBox candidaturas
     *
     * @return - comboBox devidamente criada e tratada.
     */
    private JComboBox criarComboBoxCandidaturas() {
        JComboBox cb = new JComboBox();
        cb.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (candidaturas != null) {
                    candSelecionada = (Candidatura) candidaturas.getSelectedItem();
                    controllerUi.setCandidatura(candSelecionada);
                }
            }
        });
        return cb;
    }

    /**
     * Método utilizado para preencher a comboBox candidaturas com a informação
     * correta
     */
    private void preencherComboBoxCandidaturas() {
        if (faeSelecionado != null) {
            ArrayList<Candidatura> lst = controllerUi.getListaAtribuicoesPorAvaliar(faeSelecionado);
            for (Candidatura c : lst) {
                candidaturas.addItem(c);
            }
        }
    }
    /**
     * Método utilizado para criar o painel referente ao botao de mostrar
     * candidaturas
     *
     * @return - painel devidamente criado e tratado.
     */
    private JPanel criarPainelMostrarCandidatura() {
        btnMostrarCandidatura = criarBotaoMostrarCandidatura();

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnMostrarCandidatura);
        return p;
    }

    /**
     * Método utilizado para criar o botao para mostrar a candidatura
     * selecionada
     *
     * @return - botao devidamente criado e tratado.
     */
    private JButton criarBotaoMostrarCandidatura() {
        JButton btn = new JButton("Mostrar candidatura");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (candSelecionada != null && candidaturas.getItemCount() != 0) {
                    JOptionPane.showMessageDialog(null, controllerUi.getInformacaoDaCandidaturaPorAvaliar(candSelecionada));
                } else {
                    JOptionPane.showMessageDialog(rootPane, "Não há candidaturas a mostrar");
                }
            }
        });
        return btn;
    }

    /**
     * Método utilizado para criar o painel referente ao textField da
     * justificação
     *
     * @return - painel devidamente criado e tratado.
     */
    private JPanel criarPainelJustificacao() {
        JLabel lbl1 = new JLabel("Justificação:", JLabel.RIGHT);

        justificacao = new JTextField(20);

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl1);
        p.add(justificacao);

        return p;
    }
    private JPanel criarPainelConhecimentoFAE() {
        JLabel lbl1 = new JLabel("Conhecimento do FAE sobre o tema:", JLabel.RIGHT);

        conhecimentoFAE = criarComboBoxconhecimentoFAE();
        
        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl1);
        p.add(conhecimentoFAE);

        return p;
    }
    
    private JComboBox criarComboBoxconhecimentoFAE(){
        JComboBox c1=new JComboBox();
        for(int i=0;i<6;i++){
            c1.addItem(i);
        }
        return c1;
    }

    private JPanel criarPainelAdequacaoCandidaturaExposicao() {
        JLabel lbl1 = new JLabel("Adequação das candidaturas á exposicao:", JLabel.RIGHT);

        adequacaoCandidaturaExposicao = criarComboBoxAdequacaoCandidaturaExposicao();
        
        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl1);
        p.add(adequacaoCandidaturaExposicao);

        return p;
    }
    
    private JComboBox criarComboBoxAdequacaoCandidaturaExposicao(){
        JComboBox c1=new JComboBox();
        for(int i=0;i<6;i++){
            c1.addItem(i);
        }
        return c1;
    }
    private JPanel criarPainelAdequacaoCandidaturaDemonstracoes() {
        JLabel lbl1 = new JLabel("Adequação das candidaturas á demonstração:", JLabel.RIGHT);

        adequacaoCandidaturaDemonstracoes = criarComboBoxAdequacaoCandidaturaDemonstracoes();
        
        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl1);
        p.add(adequacaoCandidaturaDemonstracoes);

        return p;
    }
    
    public JComboBox criarComboBoxAdequacaoCandidaturaDemonstracoes(){
        JComboBox c1=new JComboBox();
        for(int i=0;i<6;i++){
            c1.addItem(i);
        }
        return c1;
    }

    private JPanel criarPainelAdequacaoNrConvites() {
        JLabel lbl1 = new JLabel("Adequação dos convites:", JLabel.RIGHT);

        adequacaoNrConvites = criarComboBoxAdequacaoNrConvites();
        
        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl1);
        p.add(adequacaoNrConvites);

        return p;
    }
    
    public JComboBox criarComboBoxAdequacaoNrConvites(){
        JComboBox c1=new JComboBox();
        for(int i=0;i<6;i++){
            c1.addItem(i);
        }
        return c1;
    }
    private JPanel criarPainelRecomendacaoGlobal() {
        JLabel lbl1 = new JLabel("Recomendação global:", JLabel.RIGHT);

        recomendacaoGlobal = criarComboBoxRecomendacaoGlobal();
        
        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl1);
        p.add(recomendacaoGlobal);

        return p;
    }
    
    public JComboBox criarComboBoxRecomendacaoGlobal(){
        JComboBox c1=new JComboBox();
        for(int i=0;i<6;i++){
            c1.addItem(i);
        }
        return c1;
    }
    /**
     * Método utilizado para criar o painel referente aos botoes.
     *
     * @return - painel devidamente criado e tratado.
     */
    private JPanel criarPainelBotoes() {
        JButton btnAceitar = criarBotaoAceitar();
        getRootPane().setDefaultButton(btnAceitar);

        JButton btnCancelar = criarBotaoCancelar();
        JButton btnRejeitar = criarBotaoRejeitar();

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnAceitar);
        p.add(btnRejeitar);
        p.add(btnCancelar);

        return p;
    }

    /**
     * Método utilizado para criar o botao responsavel por guardar a decisao e a
     * justificacao da candidatura aceite pelo fae
     *
     * @return - botao devidamente criado e tratado.
     */
    private JButton criarBotaoAceitar() {
        JButton btn = new JButton("Aceitar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!justificacao.getText().equals("")) {
                    if (candidaturas.getItemCount() != 0&& conhecimentoFAE.getSelectedItem()!=null &&adequacaoCandidaturaExposicao.getSelectedItem()!=null&&adequacaoCandidaturaDemonstracoes.getSelectedItem()!=null&&adequacaoNrConvites.getSelectedItem()!=null&&recomendacaoGlobal.getSelectedItem()!=null) {
                        controllerUi.setAvaliacao(true, justificacao.getText(),(int)conhecimentoFAE.getSelectedItem(),(int)adequacaoCandidaturaExposicao.getSelectedItem(),(int)adequacaoCandidaturaDemonstracoes.getSelectedItem(),(int)adequacaoNrConvites.getSelectedItem(),(int)recomendacaoGlobal.getSelectedItem());
                        dispose();
                    } else {
                        JOptionPane.showMessageDialog(null, "Sem candidaturas!");
                    }

                } else {
                    JOptionPane.showMessageDialog(null, "Sem justificação!");
                }
            }
        });
        return btn;
    }

    /**
     * Método utilizado para criar o botao responsavel por cancelar as operações
     *
     * @return
     */
    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                faeSelecionado = null;
                candSelecionada = null;
                dispose();
            }

        });
        return btn;
    }

    /**
     * Método utilizado para criar o botao responsavel por guardar a decisao e a
     * justificacao da candidatura rejeitada pelo fae
     *
     * @return
     */
    private JButton criarBotaoRejeitar() {
        JButton btn = new JButton("Rejeitar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!justificacao.getText().equals("")) {
                    if (candidaturas.getItemCount() != 0 && conhecimentoFAE.getSelectedItem()!=null &&adequacaoCandidaturaExposicao.getSelectedItem()!=null&&adequacaoCandidaturaDemonstracoes.getSelectedItem()!=null&&adequacaoNrConvites.getSelectedItem()!=null&&recomendacaoGlobal.getSelectedItem()!=null) {
                        controllerUi.setAvaliacao(false, justificacao.getText(),(int)conhecimentoFAE.getSelectedItem(),(int)adequacaoCandidaturaExposicao.getSelectedItem(),(int)adequacaoCandidaturaDemonstracoes.getSelectedItem(),(int)adequacaoNrConvites.getSelectedItem(),(int)recomendacaoGlobal.getSelectedItem());
                        dispose();
                    } else {
                        JOptionPane.showMessageDialog(null, "Sem candidaturas!");
                    }

                } else {
                    JOptionPane.showMessageDialog(null, "Sem justificação!");
                }
            }
        });
        return btn;
    }
}
