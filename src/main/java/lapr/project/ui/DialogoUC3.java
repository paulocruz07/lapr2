package lapr.project.ui;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.border.*;
import lapr.project.model.*;
import lapr.project.controller.*;
/**
 * 
 * @author 1150855_1150856
 */

public class DialogoUC3 extends JDialog {
    /**
     * variável de instância JComboBox exposicoesDoOrganizador, para apresentar as exposicoes do organizador.
     */
    private JComboBox exposicoesDoOrganizador;
    /**
     * variável de instância JComboBox organizador, para apresentar os organizadores registados no sistema.
     */
    private JComboBox organizador;
    /**
     * variável de instância JComboBox mecanismos, para apresentar os mecanismos registados no sistema.
     */
    private JComboBox mecanismos;
    /**
     * variável de instância JTextArea listaAtribuicoes, para apresentar as atribuições geradas.
     */
    private JTextArea listaAtribuicoes;
    /**
     * variável de instância Janela framePai, para referência de onde o JDialog foi lançado.
     */
    private Janela framePai;
    /**
     * variável de instância AtribuirCandidaturaController controllerUi, para comunicar com o controller.
     */
    private AtribuirCandidaturaController controllerUi;
    /**
     * variável de classe Organizador organizadorSelecionado, para reter o organizador selecionado na JComboBox dos organizadores.
     */
    private Organizador organizadorSelecionado;
    
    /**
     * Método utilizado para construir todo o JDialog DialogoUC3
     * @param framePai - frame do qual este JDialog foi invocado.
     * @param controllerUI - controller passado por parâmetro já inicializado.
     */
    public DialogoUC3(Janela framePai,AtribuirCandidaturaController controllerUI) {
    
        super(framePai, "Atribuir candidatura", true);
        this.controllerUi=controllerUI;
        this.framePai = framePai;
     
        setLayout(new GridLayout(5,1));
        
        criarComponentes();
        
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        pack();
        setResizable(false);
        setLocationRelativeTo(framePai);
        setVisible(true);
    }
    /**
     * Método utilizado para criar todos os elementos do JDialog
     */
    private void criarComponentes(){
        JPanel p1 = criarPainelComboBoxOrganizador();
        JPanel p2 = criarPainelComboBoxExposicao();
        JPanel p3 = criarPainelComboBoxMecanismo();
        JPanel p4 = criarPainelLista();
        JPanel p5 = criarPainelBotoes();
        
        
        add(p1);
        add(p2);
        add(p3);
        add(p4);
        add(p5);
    }

    /**
     * Método utilizado para criar o painel referente á JComboBox organizador
     * @return - painel já criado e tratado.
     */
     private JPanel criarPainelComboBoxOrganizador() {
        JLabel lbl1 = new JLabel("Organizador:", JLabel.RIGHT);
        
        organizador = criarComboBoxOrganizador();
        
       
        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 3, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl1);
        p.add(organizador);

        return p;
    }
     
    /**
     * Método utilizado para criar a JComboBox organizador
     * @return - JComboBox já criada e tratada.
     */
    
     private JComboBox criarComboBoxOrganizador(){
        JComboBox cb = new JComboBox();
        cb.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                if(organizador!=null){
                    organizadorSelecionado=(Organizador) organizador.getSelectedItem();
                    if(exposicoesDoOrganizador !=null){
                        preencherComboBoxExposicao();
                    }
                }
            }
        });
       
        ArrayList<Organizador> ls = controllerUi.getListaOrganizador();
        
        for(Organizador a:ls){
            cb.addItem(a);
        }
        return cb;
    }
     /**
      * Método utilizado para criar o painel referente á JComboBox exposicoesDoOrganizador.
      * @return - painel já criado e tratado.
      */
     
    private JPanel criarPainelComboBoxExposicao() {
        JLabel lbl1 = new JLabel("Exposições do organizador:", JLabel.RIGHT); 
        
        exposicoesDoOrganizador = criarComboBoxExposicoesDoOrganizador();
        
        preencherComboBoxExposicao();
        
        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 3, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl1);
        p.add(exposicoesDoOrganizador);

        return p;
    }
    /**
     * Método utilizado para criar a JComboBox exposicoesDoOrganizador
     * @return - JComboBox já criada e tratada.
     */
    
    public JComboBox criarComboBoxExposicoesDoOrganizador(){
        JComboBox cb=new JComboBox();
        cb.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                controllerUi.setExposicao((Exposicao) exposicoesDoOrganizador.getSelectedItem());
            }
        });
        return cb;
    }
    
    /**
     * Método utilizado para preencher a comboBox exposicoesDoOrganizador com os dados referentes ao organizador selecionado.
     */
     private void preencherComboBoxExposicao(){
        if(organizadorSelecionado!=null){
            exposicoesDoOrganizador.removeAllItems();
            ArrayList<Exposicao> arrayExpo = controllerUi.getListaExposicoes(organizadorSelecionado);
            for(Exposicao a:arrayExpo){
                exposicoesDoOrganizador.addItem(a);
            }
        }
    }
     /**
      * Método utilizado para criar o painel referente á comboBox mecanismos.
      * @return - painel já criado e tratado.
      */
     
    private JPanel criarPainelComboBoxMecanismo() {
        JLabel lbl1 = new JLabel("Mecanismos:", JLabel.RIGHT);
        
         mecanismos=criarComboBoxMecanismo();
         
         
        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl1);
        p.add(mecanismos);

        return p;
    }
    
    /**
     * Método utilizado para criar a comboBox mecanismos com a devida informação.
     * @return - JComboBox já criada e preenchida
     */
    private JComboBox criarComboBoxMecanismo(){
        JComboBox cb = new JComboBox();
        
        cb.addActionListener(new ActionListener(){
            
             @Override
             public void actionPerformed(ActionEvent e){
               if(mecanismos!=null){
                String m = ""+mecanismos.getSelectedItem();
                switch(m){
                    case "MecanismoNivelExperiencia" : 
                    {
                        controllerUi.setMa(new MecanismoNivelExperiencia());
                        break;
                    }
                    case "MecanismoCargaEquitativa":
                    {
                        controllerUi.setMa(new MecanismoCargaEquitativa());
                        break;
                    }
                    case "MecanismoNrFaeCandidatura":
                        MecanismoNrFaeCandidatura mfae=new MecanismoNrFaeCandidatura();
                        
                        String nr=JOptionPane.showInputDialog("Indique o número de fae pretendidos: ");
                        try{
                            int numero=Integer.parseInt(nr);
                            mfae.setNrFaeCandidatura(numero);
                            controllerUi.setMa(mfae);
                        }catch(Exception erro){
                            JOptionPane.showMessageDialog(rootPane, "Número errado!! - "+erro.getMessage());
                        }
                        
                        break;
                }
                 
             }
        }
         });
        ArrayList<String>array=controllerUi.getListaMecanismos();
        for(String s: array){
             cb.addItem(s);
        }
        return cb;
    }
    
    /**
     * Método utilizado para criar o painel referente á listaAtribuicoes.
     * @return - painel devidamente criado e tratado
     */

    private JPanel criarPainelLista(){
        listaAtribuicoes = new JTextArea(5, 20);
        JScrollPane scrollPane = new JScrollPane(listaAtribuicoes); 
        listaAtribuicoes.setEditable(false);
        
        
        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(scrollPane);
        
        return p;
    }
    
    /**
     * Método utilizado para criar o painel referente aos botões. 
     * @return - painel devidamente criado e tratado.
     */
    private JPanel criarPainelBotoes() {
        JButton btnConfirmar = criarBotaoConfirmar();
        getRootPane().setDefaultButton(btnConfirmar);

        JButton btnCancelar = criarBotaoCancelar();
        JButton btnGerarLista= criarBotaoGerarLista();

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnGerarLista);
        p.add(btnConfirmar);
        p.add(btnCancelar);
        

        return p;
    }

    /**
     * Método utilizado para criar o botão confirmar, botão responsável por guardar as listas de atribuição geradas.
     * @return - botao devidamente criado e tratado.
     */
    private JButton criarBotaoConfirmar() {        
        JButton btn = new JButton("Confirmar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(listaAtribuicoes.getText()!=null){
                ArrayList<Atribuicao> lst = controllerUi.getListaAtribuicoes();
                controllerUi.setlAtribuicao(new ListaAtribuicoes(lst));
                controllerUi.setExposicaoAtribuida();
                dispose();
                }
            } 
        });
        
        return btn;
    }
    /**
     * Método utilizado para criar o botão cancelar, botão responsável cancelar as operações.
     * @return - botao devidamente criado e tratado.
     */
    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                exposicoesDoOrganizador.removeAllItems();
                organizador.removeAllItems();
                mecanismos.removeAllItems();
                organizadorSelecionado=null;
                dispose();
            }
        });
        return btn;
    }
    /**
     * Método utilizado para criar o botão gerarLista, botão responsável por gerar as listas de atribuição.
     * @return - botao devidamente criado e tratado.
     */
   private JButton criarBotaoGerarLista(){
       JButton btn = new JButton("Gerar Lista");  
        
        
       btn.addActionListener(new ActionListener() {
            @Override
           
            public void actionPerformed(ActionEvent e) {
                listaAtribuicoes.removeAll();
                listaAtribuicoes.setText(null);
                ArrayList<Atribuicao> lista = controllerUi.getListaAtribuicoes();
                for(Atribuicao a:lista){
                       listaAtribuicoes.append(a+"\n"); 
                }
            }
        });
        return btn;
   }

}
