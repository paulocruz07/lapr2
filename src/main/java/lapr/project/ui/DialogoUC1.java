/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import lapr.project.controller.CriarExposicaoController;
import lapr.project.controller.RegistarUtilizadorController;
import lapr.project.model.CentroExposicoes;
import lapr.project.model.Utilizador;

/**
 *
 * @author Ines Moura
 */
public class DialogoUC1 extends JDialog {

    private JLabel lblTitulo, lblDescricao, lblInicioExpo, lblFimExpo, lblInicioSub, lblFimSub, lblLocal, lblDataLimiteConflitos, lblOrg;
    private CentroExposicoes ce;
    private JTextField txtTitulo, txtDescricao, txtLocal, txtInicioExpo, txtFimExpo, txtInicioSub, txtFimSub, txtDataLimiteConflitos;
    private JButton btnRegistar, btnCancelar, btnAddOrg;
    private JComboBox cbUtilizador;
    private Janela framePai;
    private int flagOrg = 0;
    private Utilizador u = new Utilizador();
    private ArrayList<Utilizador> listaUtil;

    public DialogoUC1(Janela framePai, CriarExposicaoController controllerUI) {
        super(framePai, "Criar Exposição", true);

        this.ce = controllerUI.getCe();

        this.framePai = framePai;

        CriarExposicaoController controller = new CriarExposicaoController(ce);

        controller.novaExposicao();
        
        criarComponentes(controller);

        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();
        setMinimumSize(new Dimension(getWidth(), getHeight()));
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void criarComponentes(CriarExposicaoController controller) {
        JPanel p = criarPainelInserirDados(controller);
        add(p, BorderLayout.CENTER);
    }

    private JLabel criarLabel(String txt) {
        JLabel lbl = new JLabel(txt);
        return lbl;
    }

    private JTextField criarText() {
        JTextField txt = new JTextField();
        return txt;
    }

    private JPanel criarPainelInserirDados(CriarExposicaoController controller) {
        JPanel p = new JPanel(new GridLayout(10, 2, 10, 10));
        lblTitulo = criarLabel("Titulo: ");
        lblDescricao = criarLabel("Descrição: ");
        lblInicioExpo = criarLabel("Data inicio da exposição: ");
        lblFimExpo = criarLabel("Data fim da exposição: ");
        lblInicioSub = criarLabel("Data inicio submissão candidaturas: ");
        lblFimSub = criarLabel("Data fim submissão candidaturas: ");
        lblLocal = criarLabel("Local: ");
        lblDataLimiteConflitos = criarLabel("Data limite alteração de conflitos: ");
        txtTitulo = criarText();
        txtDescricao = criarText();
        txtInicioExpo = criarText();
        txtFimExpo = criarText();
        txtInicioSub = criarText();
        txtFimSub = criarText();
        txtLocal = criarText();
        txtDataLimiteConflitos = criarText();
        btnRegistar = criarBotaoRegistar(controller);
        btnCancelar = criarBotaoCancelar(); 
        cbUtilizador = criarComboUtilizador(controller);
        btnAddOrg = criarBotaoAddOrg(controller);
        lblOrg = criarLabel("Selecione os organizadores");
        
        JPanel p2 = new JPanel(new BorderLayout());
        
        p2.add(lblOrg,BorderLayout.NORTH);
        p2.add(cbUtilizador,BorderLayout.CENTER);

        p.add(lblTitulo);
        p.add(txtTitulo);
        p.add(lblDescricao);
        p.add(txtDescricao);
        p.add(lblInicioExpo);
        p.add(txtInicioExpo);
        p.add(lblFimExpo);
        p.add(txtFimExpo);
        p.add(lblInicioSub);
        p.add(txtInicioSub);
        p.add(lblFimSub);
        p.add(txtFimSub);
        p.add(lblLocal);
        p.add(txtLocal);
        p.add(lblDataLimiteConflitos);
        p.add(txtDataLimiteConflitos);
        p.add(p2);
        p.add(btnAddOrg);
        p.add(btnRegistar);
        p.add(btnCancelar);

        return p;
    }

    private JButton criarBotaoRegistar(CriarExposicaoController controller) {
        JButton btn = new JButton("Registar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String titulo = "", descricao = "", inicioExpo = "", fimExpo = "", inicioSub = "", fimSub = "", local = "", dataLimiteConf = "";
                try {
                    titulo = txtTitulo.getText();
                    descricao = txtDescricao.getText();
                    inicioExpo = txtInicioExpo.getText();
                    fimExpo = txtFimExpo.getText();
                    inicioSub = txtInicioSub.getText();
                    fimSub = txtFimSub.getText();
                    local = txtLocal.getText();
                    dataLimiteConf = txtDataLimiteConflitos.getText();
                } catch (Exception erro) {
                    JOptionPane.showMessageDialog(null, "É necessário preencher todos os campos");
                    return;
                }

                boolean b1 = validaData(inicioExpo);
                boolean b2 = validaData(fimExpo);
                boolean b3 = validaData(inicioSub);
                boolean b4 = validaData(fimSub);
                boolean b5 = validaData(dataLimiteConf);

                if (b1 == true && b2 == true && b3 == true && b4 == true && b5 == true) {
                    if (flagOrg == 0) {
                        JOptionPane.showMessageDialog(null, "Selecione pelo menos um organizador");
                    } else {
                        
                        controller.setDados(titulo, descricao, inicioExpo, fimExpo, inicioSub, fimSub, local, dataLimiteConf);
                        boolean b6 = controller.validaExposicao();

                        //
                        //
                        //falta validar b6 e b7
                        //
                        //
                        int dialogResult = JOptionPane.showConfirmDialog(null, "Confirmar Registo de Utilizador?", "Atenção", JOptionPane.YES_NO_OPTION);
                        if (dialogResult == 0) {
                            boolean b7 = controller.registaExposicao();
                            //nao verifica se existe outra exposicao (validacao global)
                            if (b7 == false) {
                                JOptionPane.showMessageDialog(null, "Exposição já existente");
                            } else {
                                JOptionPane.showMessageDialog(null, "Exposição criada com sucesso");
                                dispose();
                            }
                        }
                    }

                } else {
                    JOptionPane.showMessageDialog(null, "Data mal inserida\n"
                            + "Insira a data no seguinte formato: 06/04/2016");
                    return;
                }

            }
        });
        return btn;
    }

    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btn;
    }

    private JComboBox criarComboUtilizador(CriarExposicaoController controller){
        listaUtil = controller.getListaUtilizadores();
        String []temp = new String [listaUtil.size()];
        for(int i=0; i<temp.length;i++){
            temp[i] = listaUtil.get(i).getUsername();
        }
        JComboBox jcb = new JComboBox(temp);
        jcb.setSelectedIndex(0);

        return jcb;
    }

    private JButton criarBotaoAddOrg(CriarExposicaoController controller) {
        JButton btn = new JButton("Adicionar Organizador");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String username = cbUtilizador.getSelectedItem().toString();
                u = controller.getUtilizador(username);
                controller.addOrganizador(u);
                flagOrg++;
                cbUtilizador.setSelectedIndex(0);
            }
        });
        return btn;
    }

    private boolean validaData(String data) {
        int dia, mes, ano;

        int barra = data.indexOf("/");
        if (barra == 2) {
            try {
                dia = Integer.parseInt(data.substring(0, barra));
            } catch (Exception e) {
                return false;
            }
            if (dia > 31) {
                return false;
            } else {
                int barra2 = data.indexOf("/", barra + 1);
                if (barra2 - barra == 3) {
                    try {
                        mes = Integer.parseInt(data.substring(barra + 1, barra2));
                    } catch (Exception e) {
                        return false;
                    }
                    if (mes > 12) {
                        return false;
                    } else {
                        if (data.length() - barra2 == 5) {
                            try {
                                ano = Integer.parseInt(data.substring(barra2 + 1));
                            } catch (Exception e) {
                                return false;
                            }
                        } else {
                            return false;
                        }
                    }
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }

        return true;
    }
}
