package lapr.project.ui;
import java.io.File;
import javax.swing.JOptionPane;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import lapr.project.model.CentroExposicoes;
import lapr.project.model.Exposicao;
import lapr.project.model.FAE;
import lapr.project.model.FicheiroBinario;
import lapr.project.model.ListaFAE;
import lapr.project.model.ListaOrganizadores;
import lapr.project.model.Organizador;
import lapr.project.model.RegistoExposicoes;
import lapr.project.model.RegistoUtilizadores;
import lapr.project.model.Utilizador;
import lapr.project.ui.Janela;
import lapr.project.ui.RegistarUtilizadorUI;

//package lapr.project.ui;
//
//import lapr.project.model.CalculatorExample;

/**
 * @author Nuno Bettencourt <nmb@isep.ipp.pt> on 24/05/16.
 */
public class Main {

	/**
	 * Private constructor to hide implicit public one.
	 */
	private Main(){
            
	}
        public static void main(String[] args) {
            
             FicheiroBinario ficheiroBinario = new FicheiroBinario();
             CentroExposicoes ce;
             
             ce=ficheiroBinario.ler(ficheiroBinario.NOME);
//             ce=ficheiroBinario.lerXml(ficheiroBinario.NOME_XML);
            
             if(ce!=null){
                JOptionPane.showMessageDialog(null, "olaaa :"+ficheiroBinario.NOME);
             }
             if(ce==null){
                ce=new CentroExposicoes();
                Utilizador u = new Utilizador("paulo","ola@a.a","ola","eE.e4","FAE");
                RegistoUtilizadores ru = new RegistoUtilizadores();
                ru.addUtilizador(u);
                ListaOrganizadores lo=new ListaOrganizadores();
                Organizador o = new Organizador(u);
                lo.addOrganizador(o);
                Exposicao e = new Exposicao("titulo","descricao","21/12/2016","22/12/2016","25/12/2016","26/12/2016","florida","28/12/2016");
                e.setCriada();
                FAE fae = new FAE();
                fae.setUtilizador(u);
                ListaFAE lsFae=new ListaFAE();
                lsFae.addFAE(fae);
                RegistoExposicoes re=new RegistoExposicoes();
                re.addExposicao(e);
                e.setlistaOrganizadores(lo);
                ce.setListaExpo(re);
                ce.setListaUtilizadores(ru);
//            try {
//                JAXBContext context = JAXBContext.newInstance(Exposicao.class);
//                Marshaller m = context.createMarshaller();
//                m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
//                m.marshal(e, new File("Exemplo1234.xml"));
//
//            } catch (JAXBException erro) {
//            erro.printStackTrace();
//            }
//            try {
//                JAXBContext context = JAXBContext.newInstance(Exposicao.class);
//                Unmarshaller un = context.createUnmarshaller();
//                Exposicao emp = (Exposicao) un.unmarshal(new File("Exemplo1234.xml"));
//                JOptionPane.showMessageDialog(null, emp.getListaFAE().getLfae());
//            } catch (JAXBException erro) {
//                erro.printStackTrace();
//            }
//            try {
//                JAXBContext context = JAXBContext.newInstance(RegistoExposicoes.class);
//                Marshaller m = context.createMarshaller();
//                m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
//                m.marshal(re, new File("Exemplo12345.xml"));
//
//            } catch (JAXBException erro) {
//            erro.printStackTrace();
//            }
//            try {
//                JAXBContext context = JAXBContext.newInstance(RegistoExposicoes.class);
//                Unmarshaller un = context.createUnmarshaller();
//                RegistoExposicoes emp = (RegistoExposicoes) un.unmarshal(new File("Exemplo12345.xml"));
//                JOptionPane.showMessageDialog(null, emp);
//            } catch (JAXBException erro) {
//                erro.printStackTrace();
//            }
//             try {
//                JAXBContext context = JAXBContext.newInstance(ListaFAE.class);
//                Marshaller m = context.createMarshaller();
//                m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
//                m.marshal(lsFae, new File("Exemplo123456.xml"));
//
//            } catch (JAXBException erro) {
//            erro.printStackTrace();
//            }
//            try {
//                JAXBContext context = JAXBContext.newInstance(ListaFAE.class);
//                Unmarshaller un = context.createUnmarshaller();
//                ListaFAE emp = (ListaFAE) un.unmarshal(new File("Exemplo123456.xml"));
//                JOptionPane.showMessageDialog(null, "olaa : "+emp.getLfae());
//            } catch (JAXBException erro) {
//                erro.printStackTrace();
//            }
        //@XmlRootElement
             }
            
            /**
            * Inicialização da janela (JFrame)
           */
          new Janela(ficheiroBinario,ce);  
        }
}


