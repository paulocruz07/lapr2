/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import lapr.project.controller.RegistarUtilizadorController;
import lapr.project.model.CentroExposicoes;
import lapr.project.model.RegistoUtilizadores;
import lapr.project.model.Utilizador;
import lapr.project.ui.Janela;

/**
 *
 * @author Ines Moura
 */
public class RegistarUtilizadorUI extends JDialog{
    private JLabel lblUtilizador, lblPassword, lblNome, lblEmail, lblFuncao;
    private CentroExposicoes ce;
    private JTextField txtUtilizador, txtNome, txtEmail;
    private JPasswordField txtPassword;
    private JButton btnRegistar;
    private JComboBox cbFuncao;
    private Janela framePai;
    

    public RegistarUtilizadorUI(Janela framePai,RegistarUtilizadorController controllerUI) {
        super(framePai, "Registar Utilizador", true);
        this.ce = controllerUI.getCe();

        this.framePai = framePai;
        
        RegistarUtilizadorController controller = new RegistarUtilizadorController(ce);
        
        criarComponentes(controller);

        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();
        setMinimumSize(new Dimension(getWidth(), getHeight()));
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void criarComponentes(RegistarUtilizadorController controller) {
        JPanel p = criarPainelInserirDados(controller);
        add(p, BorderLayout.CENTER);
    }

    private JLabel criarLabel(String txt) {
        JLabel lbl = new JLabel(txt);
        return lbl;
    }

    private JPasswordField criarTextPassword() {
        JPasswordField txt = new JPasswordField();
        return txt;
    }

    private JTextField criarText() {
        JTextField txt = new JTextField();
        return txt;
    }

    private JPanel criarPainelInserirDados(RegistarUtilizadorController controller) {
        JPanel p = new JPanel(new GridLayout(6, 2, 10, 10));
        lblUtilizador = criarLabel("Utilizador: ");
        lblPassword = criarLabel("Password: ");
        lblFuncao = criarLabel("Função: ");
        lblNome = criarLabel("Nome: ");
        lblEmail = criarLabel("Email: ");
        txtUtilizador = criarText();
        txtPassword = criarTextPassword();
        txtNome = criarText();
        txtEmail = criarText();
        cbFuncao = criarComboFuncao();
        btnRegistar = criarBotaoRegistar(controller);

        p.add(lblUtilizador);
        p.add(txtUtilizador);
        p.add(lblPassword);
        p.add(txtPassword);
        p.add(lblNome);
        p.add(txtNome);
        p.add(lblEmail);
        p.add(txtEmail);
        p.add(lblFuncao);
        p.add(cbFuncao);
        p.add(btnRegistar);

        return p;
    }

    private JButton criarBotaoRegistar(RegistarUtilizadorController controller) {
        JButton btn = new JButton("Registar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String utilizador = txtUtilizador.getText();
                String pass = txtPassword.getText();
                String nome = txtNome.getText();
                String email = txtEmail.getText();
                String funcaoEscolhida = cbFuncao.getSelectedItem().toString();

                controller.novoUtilizador();

                boolean b1 = controller.setDadosEntrada(nome, email, utilizador, pass, funcaoEscolhida);
                
                
                if(b1==false){
                    JOptionPane.showMessageDialog(null, "Dados mal inseridos\n"
                            + "Nome: não pode conter números nem caracteres\n"
                            + "Password: é necessário pelo menos um número, letra grande, letra pequena e um dos seguintes caracteres {,.;:-}\n"
                            + "Email: (Exemplo) exemplo@email.com");
                }else{
                    int dialogResult = JOptionPane.showConfirmDialog(null, "Confirmar Registo de Utilizador?", "Atenção", JOptionPane.YES_NO_OPTION);
                    if (dialogResult == 0) {
                        boolean b2 = controller.registaUtilizador();
                        if (b2==false){
                            JOptionPane.showMessageDialog(null, "Registo já existente");
                        } else{
                            JOptionPane.showMessageDialog(null, "Registo bem sucedido");
                            dispose();
                        }
                    }
                }
            }
        });
        return btn;
    }

    private JComboBox criarComboFuncao() {
        String[] funcoes = {"FAE", "Organizador", "Representante"};
        JComboBox jcb = new JComboBox(funcoes);
        jcb.setSelectedIndex(0);

        return jcb;
    }

}
