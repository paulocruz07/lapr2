package lapr.project.ui;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.UIManager;
import javax.swing.filechooser.FileFilter;

public class MyJFileChooser extends JFileChooser {
    
    /**
     * Construtor para inicializar as variaveis do MyJFileChooser
     */
    public MyJFileChooser() {
        super();
        definirFiltroExtensaoBin();
    }
    
    /**
     * Método utilizado para definir os filtros de pesquisa
     */

    private void definirFiltroExtensaoBin() {
        setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                }
                String extensao = extensao(f);
                if (extensao != null) {
                    return extensao.equals("bin");
                }
                return false;
            }

            @Override
            public String getDescription() {
                return "*.bin";
            }

            private String extensao(File f) {
                String ext = null;
                String s = f.getName();
                int i = s.lastIndexOf(".");
                if (i != -1) {
                    ext = s.substring(i + 1).toLowerCase();
                }
                return ext;
            }
        });
    }

    /**
     * Método utilizado para personalizar a janela em portugues
     */
    public static void personalizarEmPortugues() {

        // T�tulos das Caixas de Di�logo
        UIManager.put("FileChooser.openDialogTitleText", "Importar Centro de exposicoes");
        UIManager.put("FileChooser.saveDialogTitleText", "Exportar Centro de exposicoes");

        // Bot�o "Importar"
        UIManager.put("FileChooser.openButtonText", "Importar");
        UIManager.put("FileChooser.openButtonToolTipText", "Importar Centro de exposicoes");
        
        // Bot�o "Exportar"
        UIManager.put("FileChooser.saveButtonText", "Exportar");
        UIManager.put("FileChooser.saveButtonToolTipText", "Exportar Centro de exposicoes");
        
        // Bot�o "Cancelar"
        UIManager.put("FileChooser.cancelButtonText", "Cancelar");
        UIManager.put("FileChooser.cancelButtonToolTipText", "Cancelar");
        
        // Legenda "Procurar em:"
        UIManager.put("FileChooser.lookInLabelText", "Procurar em:");
        
        // Legenda "Guardar em:"
        UIManager.put("FileChooser.saveInLabelText", "Guardar em:");
                
        // Legenda "Tipo de ficheiros:"
        UIManager.put("FileChooser.filesOfTypeLabelText", "Ficheiros do tipo:");

        // Legenda "Nome do ficheiro:"
        UIManager.put("FileChooser.fileNameLabelText", "Nome do ficheiro:");

        // Filtro "Todos os Ficheiros"
        UIManager.put("FileChooser.acceptAllFileFilterText", "Todos os Ficheiros");

        // Bot�o "Um n�vel acima"
        UIManager.put("FileChooser.upFolderToolTipText", "Um n�vel acima");

        // Bot�o "Nova Pasta"
        UIManager.put("FileChooser.newFolderToolTipText", "Criar nova pasta");

        // Bot�o "Vista Lista"
        UIManager.put("FileChooser.listViewButtonToolTipText", "Lista");

        // Bot�o "Vista Detalhada"
        UIManager.put("FileChooser.detailsViewButtonToolTipText", "Detalhes");

    }

}