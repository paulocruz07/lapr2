package lapr.project.ui;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.ArrayList;
import javax.swing.*;
import lapr.project.model.*;
import lapr.project.controller.*;

public class Janela extends JFrame {

    /**
     * variável de instância JButton btUC3, para invocar o JDialogUC3
     */
     private JButton btUC2;
    /**
     * variável de instância JButton btUC3, para invocar o JDialogUC3
     */
     private JButton btUC3;
     /**
      *  variável de instância JButton btUC3, para invocar o JDialogUC4
      */
    private JButton btUC4;
    /**
     * variável de instância JButton btUC3, para invocar o JDialogUC5
     */
    private JButton btUC5;
    /**
     * variável de instância AtribuirCandidaturaController controllerUI, para o
     * JDialogUC3 comunicar com o controller
     */

    private JButton btUC6;
    private JButton btUC7;

    private RegistoExposicoes re;

    private AtribuirCandidaturaController controllerUI;
    /**
     * variável de instância AvaliarCandidaturasController controllerUI2, para o
     * JDialogUC4 comunicar com o controller
     */
    private AvaliarCandidaturasController controllerUI2;
    /**
     * variável de instância RegistaCandidaturaController controllerUI3, para o
     * JDialogUC5 comunicar com o controller
     */
    private RegistaCandidaturaController controllerUI3;

    private ConfirmarRegistoUtilizadorController controllerUI4;

    private RegistarUtilizadorController controllerUI5;
    
    private DefinirFAEController controllerUI6;
    
    private CriarExposicaoController controllerUI7;
    /**
     * variável de instância FicheiroBinario ficheiroBinario, para guardar os
     * dados de forma persistente
     */
    private FicheiroBinario ficheiroBinario;

    /**
     * variável de instância CentroDeExposicoes ce, para guardar todos os dados
     */
    private CentroExposicoes ce;

    /**
     * Contrutor utilizado para criar todo o corpo do JFrame apresentado
     *
     * @param ficheiroBinario - nome do ficheiro binario para o qual os dados
     * vão ser guardados
     */
    public Janela(FicheiroBinario ficheiroBinario, CentroExposicoes centroDeExposicoes) {

        super("Trabalho LAPR");
        this.ficheiroBinario = ficheiroBinario;
        ce = centroDeExposicoes;
        RegistoMecanismos rm = new RegistoMecanismos();

        MecanismoCargaEquitativa m = new MecanismoCargaEquitativa();

        m.addMecanismo();
        MecanismoNivelExperiencia m1 = new MecanismoNivelExperiencia();

        m1.addMecanismo();

        MecanismoNrFaeCandidatura m2 = new MecanismoNrFaeCandidatura();

        m2.addMecanismo();

        ce.setListaMecanismos(rm);
        
            controllerUI = new AtribuirCandidaturaController(ce);
            controllerUI2=new AvaliarCandidaturasController(ce);
            controllerUI3=new RegistaCandidaturaController(ce);
            controllerUI4=new ConfirmarRegistoUtilizadorController(ce);
            controllerUI5=new RegistarUtilizadorController(ce);
            controllerUI6=new DefinirFAEController(ce);
            controllerUI7 = new CriarExposicaoController(ce);
        

        criarComponentes();

        MyJFileChooser.personalizarEmPortugues();

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                sair();
            }
        });

        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        pack();
        setMinimumSize(new Dimension(getWidth(), getHeight()));
        setLocationRelativeTo(null);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setVisible(true);
    }

    /**
     * Método utilizado para criar todos os componentes do JFrame
     */
    private void criarComponentes() {
        JMenuBar menuBar = criarBarraMenus();
        setJMenuBar(menuBar);
        
        JButton btUC1=criarBotaoUC1();
        JButton btUC2=criarBotaoUC2();
        JButton btUC3=criarBotaoUC3();
        JButton btUC4=criarBotaoUC4();
        JButton btUC5=criarBotaoUC5();
        JButton btUC7=criarBotaoUC7();
        JButton btUC6=criarBotaoUC6();
 
        
        JPanel p = new JPanel();
        
        p.add(btUC1);
        p.add(btUC2);
        p.add(btUC3);
        p.add(btUC4);
        p.add(btUC5);
        p.add(btUC6);
        p.add(btUC7);
        add(p, BorderLayout.CENTER);
    }
    
    
     private JButton criarBotaoUC1(){
         
        btUC2=new JButton("UC1-Criar Exposição");
        btUC2.setEnabled(true);
        btUC2.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                 new DialogoUC1(Janela.this,controllerUI7);
            }
        });
        return btUC2;
    }
     private JButton criarBotaoUC2(){
         
        btUC2=new JButton("UC2-Definir FAE");
        btUC2.setEnabled(true);
        btUC2.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                 new DialogoUC2(Janela.this,controllerUI6,ce.getRegistoUtilizadores().getListaUtilizadores().get(0));
            }
        });
        return btUC2;
    }
    
    /**
     * Método utilizado para criar o botao responsavel por invocar o JDialogUC3
     *
     * @return - botao devidamente criado e tratado
     */
    private JButton criarBotaoUC3() {
        btUC3 = new JButton("UC3-Atribuir candidatura");
        btUC3.setEnabled(false);
        btUC3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new DialogoUC3(Janela.this, controllerUI);
            }
        });
        return btUC3;
    }

    private JButton criarBotaoUC7() {
        btUC7 = new JButton("UC7-Confirmar Registo Utilizadores");
//        btUC7.setEnabled(false);
        btUC7.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new DialogoUC7(Janela.this, controllerUI4);
            }
        });
        return btUC7;
    }

    /**
     * Método utilizado para criar o botao responsavel por invocar o JDialogUC4
     *
     * @return - botao devidamente criado e tratado
     */
    private JButton criarBotaoUC4() {
        btUC4 = new JButton("UC4-Avaliar candidatura");
        btUC4.setEnabled(false);
        btUC4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new DialogoUC4(Janela.this, controllerUI2);
            }
        });
        return btUC4;
    }

    /**
     * Método utilizado para criar o botao responsavel por invocar o JDialogUC5
     *
     * @return - botao devidamente criado e tratado /** Método utilizado para
     * criar o botao responsavel por invocar o JDialogUC5
     * @return - botao devidamente criado e tratado
     */
    private JButton criarBotaoUC5() {
        btUC5 = new JButton("UC5-Criar candidatura");
        btUC5.setEnabled(false);
        btUC5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new DialogoUC5(Janela.this, controllerUI3);
            }
        });
        return btUC5;
    }

    private JButton criarBotaoUC6() {
        btUC6 = new JButton("UC6-Registar Utilizador");
        btUC6.setEnabled(true);
        btUC6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new RegistarUtilizadorUI(Janela.this, controllerUI5);
            }
        });
        return btUC6;
    }

    /**
     * Método utilizado para criar uma barra de menu
     *
     * @return - barra de menu devidamente criada e tratada
     */
    private JMenuBar criarBarraMenus() {
        JMenuBar menuBar = new JMenuBar();

        menuBar.add(criarMenuUtilizador());
        menuBar.add(criarMenuOpcoes());

        return menuBar;
    }

    /**
     * Método utilizado para criar o menu utilizador
     *
     * @return - menu deviamente criado e tratado
     */
    private JMenu criarMenuUtilizador() {
        JMenu menu = new JMenu("Menu");
        menu.setMnemonic(KeyEvent.VK_M);
        menu.add(criarSubMenuUtilizador());
        menu.addSeparator();
        menu.add(criarSubMenuLista());
        menu.addSeparator();
        menu.add(criarSubMenuEstatistica());
        menu.addSeparator();
        menu.add(criarItemSair());

        return menu;
    }

    /**
     * Método utilizado para criar o menu opcoes
     *
     * @return - menu deviamente criado e tratado
     */
    private JMenu criarMenuOpcoes() {
        JMenu menu = new JMenu("Op��es");
        menu.setMnemonic(KeyEvent.VK_O);

        menu.add(criarSubMenuEstilo());
        menu.addSeparator();
        menu.add(criarItemAcerca());

        return menu;
    }

    /**
     * Método utilizado para criar o submenu utilizador
     *
     * @return - submenu deviamente criado e tratado
     */
    private JMenu criarSubMenuUtilizador() {
        JMenu menu = new JMenu("Utilizador:");
        menu.setMnemonic(KeyEvent.VK_U);

        menu.add(criarItemOrganizador());
        menu.add(criarItemFae());
        menu.add(criarItemRepresentante());

        return menu;

    }

    /**
     * Método utilizado para criar o JMenuItem organizador
     *
     * @return - JMenuItem deviamente criado e tratado
     */
    private JMenuItem criarItemOrganizador() {
        JMenuItem item = new JMenuItem("Organizador", 'O');
        item.setAccelerator(KeyStroke.getKeyStroke("ctrl O"));

        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btUC3.setEnabled(true);
                btUC4.setEnabled(false);
                btUC5.setEnabled(false);

            }
        });

        return item;

    }

    private JMenuItem criarItemSair() {
        JMenuItem item = new JMenuItem("Sair", 'E');
        item.setAccelerator(KeyStroke.getKeyStroke("alt S"));

        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sair();
            }
        });

        return item;
    }

    /**
     * Método utilizado para criar o JMenuItem fae
     *
     * @return - JMenuItem deviamente criado e tratado
     */
    private JMenuItem criarItemFae() {
        JMenuItem item = new JMenuItem("Fae", 'F');
        item.setAccelerator(KeyStroke.getKeyStroke("ctrl F"));

        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btUC3.setEnabled(false);
                btUC4.setEnabled(true);
                btUC5.setEnabled(false);
            }
        });

        return item;
    }

    /**
     * Método utilizado para criar o JMenuItem representante
     *
     * @return - JMenuItem deviamente criado e tratado
     */
    private JMenuItem criarItemRepresentante() {
        JMenuItem item = new JMenuItem("Representante", 'R');
        item.setAccelerator(KeyStroke.getKeyStroke("ctrl R"));

        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btUC3.setEnabled(false);
                btUC4.setEnabled(false);
                btUC5.setEnabled(true);
            }
        });

        return item;
    }

    /**
     * Método utilizado para criar o JMenu subMenuLista
     *
     * @return - subMenu deviamente criado e tratado
     */
    private JMenu criarSubMenuLista() {
        JMenu menu = new JMenu("Lista");
        menu.setMnemonic(KeyEvent.VK_L);

        menu.add(criarItemImportarLista());
        menu.add(criarItemExportarLista());

        return menu;
    }

    /**
     * Método utilizado para criar o JMenuItem importarLista
     *
     * @return - JMenuItem deviamente criado e tratado
     */
    private JMenuItem criarItemImportarLista() {
        JMenuItem item = new JMenuItem("Importar", 'I');
        item.setAccelerator(KeyStroke.getKeyStroke("ctrl I"));

        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                MyJFileChooser fileChooser = new MyJFileChooser();
                int resposta = fileChooser.showOpenDialog(Janela.this);

                if (resposta == JFileChooser.APPROVE_OPTION) {
                    File file = fileChooser.getSelectedFile();
                    CentroExposicoes listaTelefonica = ficheiroBinario
                            .ler(file.getPath());
                    if (listaTelefonica == null) {
                        JOptionPane.showMessageDialog(
                                Janela.this,
                                "Imposs�vel ler o ficheiro: " + file.getPath() + " !",
                                "Importar",
                                JOptionPane.ERROR_MESSAGE);
                    } else {
                        JOptionPane.showMessageDialog(
                                Janela.this,
                                "Centro de exposicao importado",
                                "Importar Centro de exposições",
                                JOptionPane.INFORMATION_MESSAGE);
                    }
                }
            }
        });

        return item;
    }

    /**
     * Método utilizado para criar o JMenuItem exportarLista
     *
     * @return - JMenuItem deviamente criado e tratado
     */
    private JMenuItem criarItemExportarLista() {
        JMenuItem item = new JMenuItem("Exportar", 'X');
        item.setAccelerator(KeyStroke.getKeyStroke("ctrl X"));

        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                MyJFileChooser fileChooser = new MyJFileChooser();
                int resposta = fileChooser.showSaveDialog(Janela.this);
                if (resposta == JFileChooser.APPROVE_OPTION) {
                    File file = fileChooser.getSelectedFile();
                    if (!file.getName().endsWith(".bin")) {
                        file = new File(file.getPath().trim() + ".bin");
                    }
                    boolean ficheiroGuardado = ficheiroBinario.guardar(
                           file.getPath(),
                             ce);
                    if (!ficheiroGuardado) {
                        JOptionPane.showMessageDialog(
                                Janela.this,
                                "Imposs�vel gravar o ficheiro: "
                                + file.getPath() + " !",
                                "Exportar",
                                JOptionPane.ERROR_MESSAGE);
                    } else {
                        JOptionPane.showMessageDialog(
                                Janela.this,
                                "Ficheiro gravado com sucesso.",
                                "Exportar",
                                JOptionPane.INFORMATION_MESSAGE);
                    }
                }
            }
        });

        return item;
    }

    /**
     * Método utilizado para criar o JMenu subMenuEstilo
     *
     * @return - subMenu deviamente criado e tratado
     */
    private JMenu criarSubMenuEstilo() {
        JMenu menu = new JMenu("Estilo");
        menu.setMnemonic(KeyEvent.VK_E);

        for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
            menu.add(criarItemEstilo(info));
        }

        return menu;
    }

    /**
     * Método utilizado para criar o JMenuItem itemEstilo
     *
     * @param info - informação referente ao UIManager.LookAndFeelInfo
     * @return - JMenuItem deviamente criado e tratado
     */
    private JMenuItem criarItemEstilo(UIManager.LookAndFeelInfo info) {
        JMenuItem item = new JMenuItem(info.getName());

        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JMenuItem menuItem = (JMenuItem) e.getSource();
                try {
                    for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                        if (menuItem.getActionCommand().equals(info.getName())) {
                            UIManager.setLookAndFeel(info.getClassName());
                            break;
                        }
                    }
                    SwingUtilities.updateComponentTreeUI(Janela.this);
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(
                            Janela.this,
                            ex.getMessage(),
                            "Estilo " + menuItem.getActionCommand(),
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        return item;
    }

    /**
     * Método utilizado para criar o JMenuItem itemSair
     *
     * @return - JMenuItem deviamente criado e tratado
     */
    private JMenuItem criarSubMenuEstatistica() {

        JMenuItem item = new JMenuItem("Estatistica Keyword", 'E');
        item.setAccelerator(KeyStroke.getKeyStroke("alt S"));

        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
                re = ce.getRegistoExposicoes();
                ArrayList<Exposicao> le = re.getListaExposicoes();
                ListaCandidaturas lc = new ListaCandidaturas();
                ArrayList<Candidatura> listaCand = new ArrayList();
                ArrayList<Keyword> listaKeywords = new ArrayList();
                ArrayList<Keyword> vetorKeywords = new ArrayList();
                ArrayList<Integer> vetorNumeral = new ArrayList();
              
                for (Exposicao x : le) {
                    int cont = 0;
               
                    lc = x.getListaCandidatura();
                    listaCand = lc.getListaCandidatura();
                    for (Candidatura c : listaCand) {
                        
                        listaKeywords = c.getListaKeywords();
                         
                        for (Keyword k : listaKeywords) {
                            
                            Boolean flag = false;
                            int pos = 0;
                            for (Keyword k1:vetorKeywords) {
                               
                                if (k.equals(k1)) {
                                    flag = true;
                                    pos = vetorKeywords.indexOf(k1);
                                   
                                }
                            }
                                if (flag == true) {
                                    vetorNumeral.set(pos, vetorNumeral.get(pos)+1);
                                } else {

                                    vetorKeywords.add(k);
                                    vetorNumeral.add(1);
                                    cont++;
                                }
                        }
                    }
                }
                int total=0;
                ArrayList<Double> percentagem = new ArrayList();
                for(int x : vetorNumeral){
                    total+=x;
                }
                for(int x : vetorNumeral){
                    percentagem.add((double)x/total);
                }
                writeCSV(percentagem,vetorKeywords);
                JOptionPane.showMessageDialog(null, "Estatísticas realizadas com sucesso");
            }
        });

        return item;
    }
    
    public void writeCSV(ArrayList<Double> percentagemArray, ArrayList<Keyword> vetorKeywordsArray){
        try{
            java.io.File percentagemCSV = new java.io.File("estatisticasKeyword.csv");
            java.io.PrintWriter outfile = new java.io.PrintWriter(percentagemCSV);
            for (int i=0;i<percentagemArray.size();i++) {
                outfile.write(vetorKeywordsArray.get(i).toCSVString()+","+percentagemArray.get(i)+"\n");
            }
            outfile.close();
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Erro a criar o ficheiro das estatisticas em csv : "+e.getMessage());
        }
    }

    /**
     * Método utilizado para criar o JMenuItem itemAcerca
     *
     * @return - JMenuItem deviamente criado e tratado
     */
    private JMenuItem criarItemAcerca() {
        JMenuItem item = new JMenuItem("Acerca", 'A');
        item.setAccelerator(KeyStroke.getKeyStroke("ctrl A"));

        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(
                        Janela.this,
                        "@Copyright\nGrupo 3 - lapr 2",
                        "Acerca",
                        JOptionPane.INFORMATION_MESSAGE);
            }
        });
        return item;
    }

    /**
     * Método utilizado para guardar os dados de forma persistente no sistema.
     */
    private void sair() {
        boolean centroExposicoesGuardado
                = ficheiroBinario.guardar(FicheiroBinario.NOME,
                        ce);
        ficheiroBinario.guardarXml(ce,
                        FicheiroBinario.NOME_XML);
        if (!centroExposicoesGuardado) {
            JOptionPane.showMessageDialog(
                    Janela.this,
                    "Impossível guardar centro de exposições!",
                    "Sair",
                    JOptionPane.ERROR_MESSAGE);
        }
        dispose();
    }

}
