/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.ui;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import lapr.project.model.*;
import lapr.project.controller.*;
import lapr.project.controller.CriarStandController;

/**
 *
 * @author lapr2
 */


 public class CriarStandUI extends JDialog {
    
    /**
     * variável de instância JComboBox exposicao, para apresentar as exposicoes registadas no sistema.
     */
    private JComboBox exposicao;
    /**
     * variável de instância JTextField nome para ler o nome da empresa,morada para ler a morada da empresa,
     * telemovel para ler o telemovel da empresa, area para ler a area pretendida pela empresa,
     * qtConvites para ler a quantidade de convites pretendidos pela empresa,
     * produto para ler para uma lista de produtos, os produtos pretendidos pela empresa.
     */
    private JTextField nome, morada, telemovel, area, qtConvites, produto;
    /**
     * variável de instância JTextArea listaProdutos, para apresentar os produtos ja introduzidos.
     */
    private JTextArea listaProdutos;
    /**
     * variável de instância Janela framePai, para referência de onde o JDialog foi lançado.
     */
    private Janela framePai;
    /**
     * variável de instância JButton btnOkProduto, para introduzir na lista de produtos o produto inserido.
     */
    private JButton btnOkProduto;
    /**
     * variável de instância Exposicao exposicaoSelecionada, para reter a exposicao selecionada na comboBox das exposicoes
     */
    private Exposicao exposicaoSelecionada;
    /**
     * variável de instância RegistaCandidaturaController controllerUi, para comunicar com o controller
     */
    private final CriarStandController controllerUi;
    
    /**
     * Método utilizado para construir todo o JDialog DialogoUC5
     * @param framePai - frame do qual este JDialog foi invocado.
     * @param controllerUi - controller passado por parâmetro já inicializado.
     */
   
    public CriarStandUI(Janela framePai,CriarStandController controllerUi) {
    
        super(framePai, "Criar Stand", true);

        this.framePai = framePai;
     
        setLayout(new GridLayout(5,2));
        this.controllerUi=controllerUi;
        
        criarComponentes();
        
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        pack();
        setResizable(false);
        setLocationRelativeTo(framePai);
        setVisible(true);
    }
    /**
     * Método utilizado para criar todos os elementos do JDialog
     */
    
    private void criarComponentes(){
        JPanel p1 = criarPainelExposicao();
        JPanel p2 = criarPainelNomeStand();
        JPanel p3 = criarPainelArea();
        JPanel p9 = criarPainelBotoes();

        
        add(p1);
        add(p2);
        add(p3);
        add(p9);
    }
    
    /**
     * Método utilizado para criar o painel referente á JComboBox exposicao
     * @return - painel devidamente criado e tratado
     */
    private JPanel criarPainelExposicao(){
        JPanel p=new JPanel();
        JLabel lbl1 = new JLabel("Exposicao:", JLabel.RIGHT);
        exposicao = criarComboBoxExposicao();
        
        
        final int MARGEM_SUPERIOR = 3, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl1);
        p.add(exposicao);
        return p;
    }

    /**
     * Método utilizado para criar a comboBox exposicao
     * @return - comboBox devidamente criada e tratada.
     */
    private JComboBox criarComboBoxExposicao(){
        JComboBox cb=new JComboBox();
        cb.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                if(exposicao!=null){
                    exposicaoSelecionada=(Exposicao) exposicao.getSelectedItem();
                    controllerUi.setExposicao(exposicaoSelecionada);
                }
            }
        });
        
        ArrayList<Exposicao> array=controllerUi.getListaExposicoes();
        for(Exposicao e:array){
            cb.addItem(e);
        }
        return cb;
    }
    
    /**
     * Método utilizado para criar o painel referente á TextField nome
     * @return - painel devidamente criado e tratado.
     */
    
     private JPanel criarPainelNomeStand() {
        JLabel lbl1 = new JLabel("Nome do Stand:", JLabel.RIGHT);
        nome = new JTextField(10);
        
        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 3, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl1);
        p.add(nome);

        return p;
    }
     
     /**
     * Método utilizado para criar o painel referente á TextField area
     * @return - painel devidamente criado e tratado.
     */
     private JPanel criarPainelArea(){
       JLabel lbl1 = new JLabel("Área do Stand:", JLabel.RIGHT); 
        
        area = new JTextField(10);
        
        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 3, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl1);
        p.add(area);

        return p;
    }
   
     
     
      /**
     * Método utilizado para criar o painel referente aos botoes
     * @return - painel devidamente criado e tratado.
     */
    private JPanel criarPainelBotoes() {
        JButton btnConfirmar = criarBotaoConfirmar();
        getRootPane().setDefaultButton(btnConfirmar);

        JButton btnCancelar = criarBotaoCancelar();
        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnConfirmar);
        p.add(btnCancelar);

        return p;
    }
    
    /**
     * Método utilizado para criar o botao responsavel por guardar os dados introduzidos referentes á candidatura
     * na exposicao selecionada
     * @return - JButton devidamente criado e tratado.
     */
    private JButton criarBotaoConfirmar() {        
        JButton btn = new JButton("Confirmar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               try{
                Stand s =new Stand(nome.getText(),Double.parseDouble(area.getText()));
                
                controllerUi.getExposicao().adicionaStand(s);
                dispose();
               }catch(NumberFormatException err)   {
                   JOptionPane.showMessageDialog(null,"O formato dos parâmetros do tipo inteiro não é válido. " + err.getMessage());
                   
               }catch(Exception er){
                JOptionPane.showMessageDialog(null,"Erro: " + er.getMessage());
                }
            } 
        });
        
        return btn;
    }
    
     private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btn;
    }
    
 }

