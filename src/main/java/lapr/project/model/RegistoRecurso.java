/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lapr grupo 3
 */
@XmlRootElement
public class RegistoRecurso implements Serializable{
    
    private Recurso r;
    private ArrayList<Recurso> listaRecursos;
     
    public RegistoRecurso(ArrayList<Recurso> listaRecursos){
         this.listaRecursos=new ArrayList<>(listaRecursos);
     }
     
    public RegistoRecurso(){
         this.listaRecursos=new ArrayList<>();
     }

    /**
     * @return the listaRecursos
     */
    
    public ArrayList<Recurso> getListaRecursos() {
        return listaRecursos;
    }

    /**
     * @param listaRecursos the listaRecursos to set
     */
    
    public void setListaRecursos(ArrayList<Recurso> listaRecursos) {
        this.listaRecursos = listaRecursos;
    }
     
    public Recurso novoRecurso(String descricao){
         return new Recurso(descricao);
     }
    
    public boolean registaRecurso(Recurso r){
         boolean vr=validaRecurso(r);
        if(vr){
            addRecurso(r);
        }
        return vr;
        
    }
    
    private boolean validaRecurso(Recurso r){
       for (Recurso  recursoTemp: listaRecursos){
            if((recursoTemp.getDescricao().equalsIgnoreCase(r.getDescricao())));
            return false;
        }
        return true;
        
        
    }
    
    private void addRecurso(Recurso r){
        listaRecursos.add(r);
    }
     
    
}
