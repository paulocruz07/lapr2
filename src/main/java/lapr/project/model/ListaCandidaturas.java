/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlRootElement;
import lapr.project.model.Candidatura;

/**
 *
 * @author 1150855_1150856
 */
@XmlRootElement
public class ListaCandidaturas implements Serializable {

    private ArrayList<Candidatura> listaCandidatura;

    public ListaCandidaturas(ArrayList<Candidatura> lstCandidaturas) {
        this.listaCandidatura = new ArrayList<>(lstCandidaturas);
    }

    public ListaCandidaturas() {
        listaCandidatura = new ArrayList();
    }

    /**
     * Devolve lista de candidaturas
     *
     * @return the listaCandidatura
     */
    public ArrayList<Candidatura> getListaCandidatura() {
        return listaCandidatura;
    }

    /**
     * Atualiza lista candidatura
     *
     * @param listaCandidatura - Variavel do tipo ArrayList recebida por
     * parametro
     */
    public void setListaCandidatura(ArrayList<Candidatura> listaCandidatura) {
        this.listaCandidatura = listaCandidatura;
    }

    /**
     * Adiciona caniddatura
     *
     * @param c - Variavel do tipo Candidatura recebida por parametro
     * @return
     */
    public boolean add(Candidatura c) {
        return listaCandidatura.add(c);
    }

    public void getListaCandidaturasAceites() {

    }

}
