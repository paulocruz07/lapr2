/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jorge
 */
@XmlRootElement
public interface CandidaturaEstado {
    
    public boolean valida();
    
    public boolean setEmSubmissao();
    
    public boolean setEmAvaliacao();
    
    public boolean setRemovida();
    
    public boolean setNaoAvaliada();
    
    public boolean setAvaliada();
    
    public boolean setAceite();
    
    public boolean setRejeitada();
    
    public boolean setTerminada();
    
    public boolean setStandsAtribuidos();
    
    
}
