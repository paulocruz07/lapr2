/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jorge
 */
@XmlRootElement
public class SuperExposicao implements ExposicaoEstado,Serializable {

    @Override
    public boolean valida() {
        return false;
    }
    

    @Override
    public boolean setCriada() {
        return false;
    }

    @Override
    public boolean setFaeDefinido() {
        return false;
    }

    @Override
    public boolean setDemonstracaoDefinida() {
        return false;
    }
    
    
    @Override
    public boolean setCandidaturaAberta(){
        return false;
    }
    
    @Override
    public boolean setCandidaturaTerminada(){
        return false;
    }
    
    @Override
    public boolean setConflitosDetetados(){
        return false;
    }
    
    @Override
    public boolean setConflitosAlterados(){
        return false;
    }
    
    @Override
    public boolean setCandidaturasAtribuidas(){
        return false;
    }
    
    @Override
    public boolean setCandidaturasAvaliadas(){
        return false;
    }
    
    @Override
    public boolean setCandidaturasDecididas(){
        return false;
    }
    
    @Override
    public boolean setTerminada(){
        return false;
    }
    
    
    
}
