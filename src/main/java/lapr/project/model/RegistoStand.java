/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lapr grupo 3
 */
@XmlRootElement
public class RegistoStand implements Serializable{
    
    
    private ArrayList<Stand> listaStands;
    
    public RegistoStand(ArrayList<Stand> listaStands){
        this.listaStands=new ArrayList<>(listaStands);
    }
    
    public RegistoStand(){
        this.listaStands=new ArrayList<>();
    }
    
     /**
     * @return the listaStands
     */
    
    public ArrayList<Stand> getListaStands() {
        return listaStands;
    }

    /**
     * @param listaStands the listaStands to set
     */
    
    public void setListaStands(ArrayList<Stand> listaStands) {
        this.listaStands = listaStands;
    }
    
    public Stand novoStand(String nome, double area){
        return new Stand(nome,area);
    }
    
    public boolean RegistoStand(Stand s){
        boolean st=validaStand(s);
        if(st){
            addStand(s);
        }
        return st;     
    }
    
    public void addStand(Stand s){
       listaStands.add(s);
    }

    private boolean validaStand(Stand s){
        for (Stand Standtemp : listaStands){
            if((Standtemp.getNome().equalsIgnoreCase(s.getNome())))
            return false;
        }
        return true;
    }  
}
