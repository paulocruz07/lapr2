/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jorge
 */
@XmlRootElement
public class ExposicaoEstadoCandidaturasAvaliadas extends SuperExposicao implements Serializable{
    
    Exposicao exp;

    public ExposicaoEstadoCandidaturasAvaliadas(Exposicao e) {
        this.exp = e;
    }

     @Override
    public boolean setCandidaturasDecididas() {
        if (valida()) {
            exp.setEstado(new ExposicaoEstadoCandidaturasDecididas(exp));
            return true;
        }
        return false;
    }



    @Override
    public boolean valida() {
        return true;
    }
}