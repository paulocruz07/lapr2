/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author tiago
 */
@XmlRootElement
public class Demonstracao implements Serializable{
    
    private String codigo;
    private String descricao;
    private ListaRecursosDemonstracao recursosDemonstracao;
    
    private static int contadorDemonstracoes=0;
    private static final  String prefixo_codigo="demo0011-";
    
   
    
    public Demonstracao(String codigo, String demonstracao, ListaRecursosDemonstracao recursosDemonstracao){
        this.descricao=descricao;
        this.recursosDemonstracao=recursosDemonstracao;
        contadorDemonstracoes++;
        setCodigo(prefixo_codigo+contadorDemonstracoes);
    }
    public Demonstracao(){
        contadorDemonstracoes++;
        setCodigo(prefixo_codigo+contadorDemonstracoes);
    }

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
     /**
     * @return the recursosDemonstracao
     */
    public ListaRecursosDemonstracao getRecursosDemonstracao() {
        return recursosDemonstracao;
    }

    /**
     * @param recursosDemonstracao the recursosDemonstracao to set
     */
    public void setRecursosDemonstracao(ListaRecursosDemonstracao recursosDemonstracao) {
        this.recursosDemonstracao = recursosDemonstracao;
    }
    
 
    @Override
    public String toString(){
        return "Codigo: "+codigo+", descricão: "+descricao+", recurso da demonstracao: "+recursosDemonstracao+".";
    }
   
}
