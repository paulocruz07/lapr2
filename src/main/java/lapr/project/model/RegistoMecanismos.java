/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author 1150855_1150856
 */
@XmlRootElement
public class RegistoMecanismos implements Serializable{
    /**
     * Variavel de classe ArrayList listaMEcanimos
     */
    private static ArrayList<String> listaMecanismos;
   
    /**
     * Construtor sem parametros
     */
    public RegistoMecanismos(){
        listaMecanismos=new ArrayList();
    }
    
     /**
     * Devolve a lista de mecanismos
     * @return 
     */
    public ArrayList<String> getListaMecanismos(){
       return listaMecanismos;    
    }
    
    /**
     * Adiciona mecanismos
     * @param m - Variavel do tipo String recebida por parametro
     */
    public static void addMecanismo(String m){
        listaMecanismos.add(m);
    }
}
