/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author 1150855_1150856
 */
@XmlRootElement
public interface MecanismoAtribuicao extends Serializable{
    /**
     * Metodo abstrato ListaAtribuicoes
     * @param e
     * @return 
     */
    public abstract ArrayList<Atribuicao> getListaAtribuicoes(Exposicao e);
    /**
     * Metodo abstrato addMecanismo
     */
    public abstract void addMecanismo();
}
