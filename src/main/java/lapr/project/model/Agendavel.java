/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lapr grupo 3
 */
@XmlRootElement
public interface Agendavel {
    public abstract void schedule(Object task,String data);
}
