/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lapr grupo 3
 */
@XmlRootElement
public class Organizador implements Serializable{
    private Utilizador u;
    public Organizador(Utilizador u){
        this.u=u;
    }
    public Organizador(){
        
    }
      public Utilizador getUtilizador(){
        return u;
    }
    public void setUtilizador(Utilizador u){
        this.u=u;
    }
  
    @Override
    public String toString(){
        return u.toString();
    }
}
