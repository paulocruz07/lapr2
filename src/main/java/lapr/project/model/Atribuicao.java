/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author 1150855_1150856
 */
@XmlRootElement
public class Atribuicao implements Serializable{
    /**
     * Variavel de instancia FAE fae
     */
    private FAE fae;
    /**
     * Variavel de instancia Candidatura c
     */
    private Candidatura c;
    
    /**
     * Contrutor com parametros FAE fae e Candidatura c
     * @param fae - variavel do tipo FAE passada por parametro
     * @param c  - variavel do tipo Candidatura passada por parametro
     */
    public Atribuicao(FAE fae,Candidatura c){
        this.fae=fae;
        this.c=c;
    }

    /**
     * Retorna fae
     * @return the fae
     */
    public FAE getFae() {
        return fae;
    }

    /**
     * Retorna Candidatura
     * @return the c
     */
    public Candidatura getC() {
        return c;
    }

    /**
     * Atualiza fae
     * @param fae - variavel do tipo FAE passada por parametro
     */
    public void setFae(FAE fae) {
        this.fae = fae;
    }

    /**
     * Atualiza Candidatura
     * @param c - variavel do tipo Candidatura passada por parametro
     */
    public void setC(Candidatura c) {
        this.c = c;
    }
    /**
     * Retorna uma string com as variaveis de instancia
     * @return 
     */
    @Override
    public String toString(){
        return "Fae: "+fae+", candidatura: " + c;
    }
    
}
