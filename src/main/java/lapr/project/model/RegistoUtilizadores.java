/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lapr grupo 3
 */
@XmlRootElement
public class RegistoUtilizadores implements Serializable{
    private ArrayList<Utilizador> listaUtilizadores;

    public RegistoUtilizadores(ArrayList<Utilizador> listaUtilizadores){
        this.listaUtilizadores=new ArrayList<>(listaUtilizadores);
    }
    public RegistoUtilizadores(){
        this.listaUtilizadores=new ArrayList();
    }
    
    
    /**
     * @return the listaUtilizadores
     */
    public ArrayList<Utilizador> getListaUtilizadores() {
        return listaUtilizadores;
    }

    public Utilizador getUtilizador(String username){
        for(Utilizador u: listaUtilizadores){
            if(u.contemUsername(username)){
                return u;
            }
        }
        return null;
    }
    
    /**
     * @param listaUtilizadores the listaUtilizadores to set
     */
    public void setListaUtilizadores(ArrayList<Utilizador> listaUtilizadores) {
        this.listaUtilizadores = listaUtilizadores;
    }
    
    public Utilizador novoUtilizador(){
        Utilizador u = new Utilizador();
        return u;
    }
    
    public boolean registaDados(Utilizador u){
        boolean b = validaUtilizador(u);
        if(b)
            addUtilizador(u);
        
        return b;
    }
    
    
    public boolean validaUtilizador(Utilizador u){
        for (Utilizador temp : listaUtilizadores) {
            if ((temp.getUsername().equalsIgnoreCase(u.getUsername())) || (temp.getEmail().equalsIgnoreCase(u.getEmail()))){
                return false;
            }
        }

        return true;
    }
    
    public void addUtilizador(Utilizador u){
        listaUtilizadores.add(u);
    }
    
}
