/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jorge
 */
@XmlRootElement
public class ExposicaoEstadoTerminada extends SuperExposicao implements Serializable{
    
    Exposicao exp;

    public ExposicaoEstadoTerminada(Exposicao e) {
        this.exp = e;
    }
}