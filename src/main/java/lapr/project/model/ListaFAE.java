/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jorge
 */
@XmlRootElement
public class ListaFAE implements Serializable{
    private ArrayList<FAE> lfae;
    
    public ListaFAE(ArrayList<FAE> lfae){
        this.lfae=new ArrayList(lfae);
    }
    
    public ListaFAE(){
        this.lfae = new ArrayList<FAE>();
    }
     
     public void setFAE(ArrayList<FAE> lf){
         this.lfae=new ArrayList(lf);
     }

    /**
     * @return the lfae
     */
    public ArrayList<FAE> getLfae() {
        return lfae;
    }
    
    
     
    public FAE addCriaFAE(Utilizador u, Exposicao e){
        FAE fae = new FAE();
        fae.setUtilizador(u);
        fae.setNivelExperiência(2);
        fae.validaFAE();
        return fae;
    }
    
    public boolean registarFAE(FAE fae){
        if(validaFAE(fae)){
           addFAE(fae);
           return true;
        }
        return false;
    }
    
     public FAE getFAE(FAE fae) {
        return getLfae().get(getLfae().indexOf(fae));
    }
    public boolean validaFAE(FAE fae){
        boolean flag = false;
        for(FAE fae1 : lfae){
            if(fae.getUtilizador().getUsername().equals(fae1.getUtilizador().getUsername())){
                flag=true;
            }
        }
        if(flag){
            return false;
        }else{
            return true;
        }
//        if(getLfae().contains(fae)){
//            JOptionPane.showMessageDialog(null, "Falhou");
//            return false;
//            
//        }
//        return true;
    }
    public boolean addFAE(FAE fae){
        getLfae().add(fae);
        return true;
    }
   public String toString(){
       return "Lista de FAE: "+lfae;
   }
    
}
