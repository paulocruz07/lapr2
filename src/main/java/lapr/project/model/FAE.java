/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jorge
 */
@XmlRootElement
public class FAE implements Serializable{
    private Utilizador m_oUFae;
    private Integer nivelExperiência;

    private final static int NIVEL_POR_OMISSAO = 1;
    
    public FAE() {
        this.m_oUFae=new Utilizador();
        this.nivelExperiência = NIVEL_POR_OMISSAO;
    }

    public FAE(Utilizador u, Integer nivelExperiência) {
        this.m_oUFae = u;
        this.nivelExperiência = nivelExperiência;
    }
    
      public FAE(Utilizador u) {
        this.m_oUFae = u;
        this.nivelExperiência = NIVEL_POR_OMISSAO;
    }

    /**
     * @return the nivelExperiência
     */
    public Integer getNivelExperiencia() {
        return nivelExperiência;
    }

    public Utilizador getUtilizador() {
        return m_oUFae;
    }

    /**
     * @param nivelExperiência the nivelExperiência to set
     */
    public void setNivelExperiência(Integer nivelExperiência) {
        this.nivelExperiência = nivelExperiência;
    }

    public void setUtilizador(Utilizador u) {
        this.m_oUFae = u;
    }

    
    public boolean validaFAE() {
        return true;
    }

    public boolean valida() {
        // Introduzir as validações aqui
        return true;
    }

    @Override
    public String toString() {
        return this.m_oUFae != null ? this.m_oUFae.toString() : null;
    }

}
