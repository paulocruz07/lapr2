    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lapr grupo 3
 */
@XmlRootElement
public class ExposicaoEstadoCriada extends SuperExposicao implements Serializable{
    
    Exposicao exp;
    
    public ExposicaoEstadoCriada(Exposicao e){
        this.exp = e;
    }
    
    @Override
    public boolean setFaeDefinido(){
        if(valida()){
            exp.setEstado(new ExposicaoEstadoFaeSemDemonstracao(exp));
            return true;
        }
        return false;
    }
    
    public boolean setDemonstracao(){
        if(valida()){
            exp.setEstado(new ExposicaoEstadoDemonstracaoSemFAEDefinido(exp));
            return true;
        }
        return false;
    }
    
    
    
    @Override
    public boolean valida(){
        return true;
    }
}
