package lapr.project.model;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.swing.JOptionPane;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import lapr.project.utils.XMLParser;
import org.xml.sax.*;
import org.w3c.dom.*;

@XmlRootElement
public class FicheiroBinario implements Serializable{
    
    /**
     * Constante NOME
     */
    public static final String NOME = "ficheiroBinario.bin";
    public static final String NOME_XML="ficheiroXml.xml";
   
    
    
//    public boolean guardarExpo(Exposicao exposicao, File fileName) {
//        try {
//            if (fileName == null) {
//                fileName = new File(NOME);
//            }
//            JAXBContext context;
//            context = JAXBContext.newInstance(Exposicao.class);
// 
//            //Criar XML
//            Marshaller marshal = context.createMarshaller();
//            marshal.marshal(exposicao, fileName);
//            return true;
//        } catch (Exception e) {
//            JOptionPane.showMessageDialog(null,"Erro a exportar para xml (Exposicao): " + e.getMessage());
//            return false;
//        }
//    }
// 
//    public Exposicao lerExpo(File fileName) {
//        try {
//            if (fileName == null) {
//                fileName = new File(NOME);
//            }
//            JAXBContext context;
//            context = JAXBContext.newInstance(Exposicao.class);
// 
//            //Ler XML
//            Unmarshaller notMarshal = context.createUnmarshaller();
//            return (Exposicao) notMarshal.unmarshal(fileName);
// 
//        } catch (Exception e) {
//            JOptionPane.showMessageDialog(null,"Erro a importar para xml (Exposicao): " + e.getMessage());
//            return null;
//        }
//       
//    }
//    public CentroExposicoes ler(String filename) {
//        try {
//            JAXBContext context = JAXBContext.newInstance(CentroExposicoes.class);
//            Unmarshaller un = context.createUnmarshaller();
//            CentroExposicoes emp = (CentroExposicoes) un.unmarshal(new File(filename));
//            return emp;
//        } catch (Exception erro) {
//            erro.printStackTrace();
//        }
//        return null;
//    }
// 
// 
//    public  boolean guardar(CentroExposicoes emp,String filename) {
// 
//        try {   
//            JAXBContext context = JAXBContext.newInstance(CentroExposicoes.class);
//            Marshaller m = context.createMarshaller();
//            //for pretty-print XML in JAXB
//            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
// 
//            // Write to System.out for debugging
//            // m.marshal(emp, System.out);
// 
//            // Write to File
//            m.marshal(emp, new File(filename));
//            return true;
//        } catch (JAXBException e) {
//            e.printStackTrace();
//        }
//        return false;
//    }
 
    
    public boolean guardarXml(CentroExposicoes f, String filename){
         XMLEncoder encoder=null;
		try{
                    encoder=new XMLEncoder(new BufferedOutputStream(new FileOutputStream(filename)));
                    encoder.writeObject(f);
                    encoder.close();
		}catch(Exception erro){
			System.out.println("ERROR: While Creating or Opening the File dvd.xml");
                        return false;
		}
                return true;
    }
   
    public CentroExposicoes lerXml(String filename){
      try{
        XMLDecoder decoder =
            new XMLDecoder(new BufferedInputStream(
                new FileInputStream(filename)));
        CentroExposicoes o = (CentroExposicoes)decoder.readObject();
        decoder.close();
        return o;
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Impossivel guardar centroExposicoes");
        }
        return null;
	
    }
//    public boolean guardarXml(CentroExposicoes f, String filename){
//        try{
//        XMLEncoder encoder =
//           new XMLEncoder(
//              new BufferedOutputStream(
//                new FileOutputStream(filename)));
//        encoder.writeObject(f);
//        encoder.close();
//        return true;
//        }catch(Exception e){
//            JOptionPane.showMessageDialog(null, "Impossivel guardar centroExposicoes");
//             return false;
//        }
//       
//    }
//
//    public CentroExposicoes lerXml(String filename){
//        try{
//        XMLDecoder decoder =
//            new XMLDecoder(new BufferedInputStream(
//                new FileInputStream(filename)));
//        CentroExposicoes o = (CentroExposicoes)decoder.readObject();
//        decoder.close();
//        return o;
//        }catch(Exception e){
//            JOptionPane.showMessageDialog(null, "Impossivel ler centroExposicoes");
//        }
//        return null;
//    }
//    /**
//     * Le ficheiro binario
//     * @param nomeFicheiro - Variavel do tipo String recebida por parametro
//     * @return 
//     */
    public CentroExposicoes ler(String nomeFicheiro) {
        
        CentroExposicoes ce;
        try {
            ObjectInputStream in = new ObjectInputStream(
                    new FileInputStream(nomeFicheiro));
            try {
                ce = (CentroExposicoes) in.readObject();
            } finally {
                in.close();
            }
            return ce;
        } catch (IOException | ClassNotFoundException ex) {
            return null;
        }
    }

    /**
     * Guarda no ficheiro em binario
     * @param nomeFicheiro - Variavel do tipo String recebida por parametro
     * @param ce - Variavel do tipo CentroDeExposicoes recebida por parametro
     * @return 
     */
    public boolean guardar(String nomeFicheiro, CentroExposicoes ce) {
        try {
            ObjectOutputStream out = new ObjectOutputStream(
                    new FileOutputStream(nomeFicheiro));
            try {
                out.writeObject(ce);
            } finally {
                out.close();
            }
            return true;
        } catch (IOException ex) {
            return false;
        }
    }

}
