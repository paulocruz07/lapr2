/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.util.Random;
import java.io.Serializable;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lapr grupo 3
 */
@XmlRootElement
public class Utilizador implements Serializable{
   /**
     * Variavel de instancia String nome
     */
    private String nome;
    private boolean confirmacao;
    /**
     * Variavel de instancia String email
     */
    private String email;
    /**
     * Variavel de instancia String username
     */
    private String username;
    /**
     * Variavel de instancia String password
     */
    private String password;
    /**
     * Variavel de instancia Boolean registado
     */
    private Boolean registado;
    /**
     * Variavel de instancia String funcao
     */
    private String funcao;
    /**
     * Variavel de instancia String funcao
     */
    private int nCod;
    
//    private static final String NOME_POR_OMISSAO="sem nome";
//    private static final String EMAIL_POR_OMISSAO="22@s.s";
//    private static final String USERNAME_POR_OMISSAO="sem username";
//    private static final String FUNCAO_POR_OMISSAO="FAE";
    
    public Utilizador(String nome,String email,String username,String password, String funcao){
        this.nome=nome;
        this.email=email;
        this.username=username;
        this.password=password;
        this.funcao = funcao;
        this.registado=false;
        this.nCod = 0;
        confirmacao=false;
    }
    
    public Utilizador(){
//        this.nome=NOME_POR_OMISSAO;
//        this.email=EMAIL_POR_OMISSAO;
//        this.username=USERNAME_POR_OMISSAO;
//        this.password=PASSWORD_POR_OMISSAO;
//        this.funcao = FUNCAO_POR_OMISSAO;
//        this.registado=false;
//        this.nCod = 0;
//        confirmacao=false;
    }
    
    /**
     * Verifica se contem id
     * @param strId - Variavel do tipo String recebida por parametro
     * @return 
     */
     public boolean contemUsername(String strId)
    {
        return getUsername().equalsIgnoreCase(strId);
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }
    
    /**
     * @return the funcao
     */
    public String getFuncao() {
        return funcao;
    }

    /**
     * @return the registado
     */
    public Boolean getRegistado() {
        return registado;
    }
    
    /**
     * @return the registado
     */
    public int getNCod() {
        return nCod;
    }
    public boolean getConfirmado(){
        return confirmacao;
    }

    /**
     * @param nome the nome to set
     * @return 
     */
    public boolean setNome(String nome) {
        boolean b = validacaoNome(nome);
        if(b){
            this.nome = nome;
            return true;
        }
        else
            return false;
    }

    /**
     * @param email the email to set
     * @return 
     */
    public boolean setEmail(String email) {
        boolean b = validacaoEmail(email);
        if(b){
            this.email = email;
            return true;
        }
        else
            return false;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }
    
    /**
     * @param funcao the funcao to set
     */
    public void setFuncao(String funcao) {
        this.funcao = funcao;
    }

    /**
     * @param password the password to set
     * @return 
     */
    public boolean setPassword(String password) {
        boolean b = validacaoPass(password);
        if(b){
            String passFinal = EncriptarPassword(password);
            System.out.println("inicial: " + password + "   final: " + passFinal);
            this.password = passFinal;
            return true;
        }
        else
            return false;
    }

    /**
     * @param registado the registado to set
     */
    public void setRegistado(Boolean registado) {
        this.registado = registado;
    }
    public void setConfirmado(){
        this.confirmacao = true;
    }
    
    /**
     * @param nCod the nCod to set
     */
    public void setNCod(int nCod) {
        this.nCod = nCod;
    }
    
    
    
    
     
     
    private boolean validacaoPass(String pass) {
        String chars2 = ",.;:-";
        String pattern2 = ".*[" + Pattern.quote(chars2) + "].*";

        if (!(pass.matches(".*[0-9].*") && pass.matches(".*[A-Z].*") && pass.matches(".*[a-z].*") && pass.matches(pattern2))) {
            JOptionPane.showMessageDialog(null, "Password inválida");
            return false;
        }
        
        return true;
    }
    
    private boolean validacaoNome(String nome){
        String chars = "!@#$%^&*()_";
        String pattern = ".*[" + Pattern.quote(chars) + "].*";
        if (nome.matches(pattern) || nome.matches(".*\\d.*")) {
            JOptionPane.showMessageDialog(null, "Nome inválido");
            return false;
        }
        
        return true;
    }
    
    private boolean validacaoEmail(String email){
        int posArroba = email.indexOf("@");
        int posPonto = email.lastIndexOf(".");
        if (posArroba == -1 || posPonto == -1) {
            JOptionPane.showMessageDialog(null, "Email inválido");
            return false;
        } else if (posArroba > posPonto) {
            JOptionPane.showMessageDialog(null, "Email inválido");
            return false;
        }

        return true;
    }
    
    
    
    private String EncriptarPassword(String pass) {
        Random random = new Random();
        int n = random.nextInt(67)+1 , flag = 0;
        System.out.println("Random: " + n);
        String passFinal = new String();
        char[] pass2 = pass.toCharArray();
        char[] abc = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ',', '.',';',':','-'};
        char[] cod = new char[abc.length];

        for (int i = 0; i < cod.length; i++) {
            if (i + n > cod.length-1) {
                cod[i] = abc[flag];
                flag++;
            } else {
                cod[i] = abc[i + n];
            }
        }
        

        for (int i = 0; i < pass.length(); i++) {
            for (int j = 0; j < cod.length; j++) {
                if (pass2[i] == abc[j]) {
                    passFinal = passFinal + cod[j];
                }

            }
        }

        setNCod(n);
        
        return passFinal;
    }
    @Override
    public String toString(){
        return "nome: "+nome+", emai: " + email;
    }
}
