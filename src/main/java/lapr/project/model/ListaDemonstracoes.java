/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author tiagoemanuelgomesnov
 */
@XmlRootElement
public class ListaDemonstracoes implements Serializable{
    
    private ArrayList<Demonstracao> listaDemonstracoes;
    private Demonstracao demonstracao;
    
    public ListaDemonstracoes(ArrayList<Demonstracao> listaDemonstracoes){
        this.listaDemonstracoes=listaDemonstracoes;
    }
    
    public ListaDemonstracoes(){
        
    }

    /**
     * @return the listaDemonstracoes
     */
    public ArrayList<Demonstracao> getListaDemonstracoes() {
        return listaDemonstracoes;
    }

    /**
     * @param listaDemonstracoes the listaDemonstracoes to set
     */
    public void setListaDemonstracoes(ArrayList<Demonstracao> listaDemonstracoes) {
        this.listaDemonstracoes = listaDemonstracoes;
    }
    
    public Demonstracao novaDemonstracao(String codigo, String descricao, ListaRecursosDemonstracao recursosDemonstracao){
        return new Demonstracao(codigo,descricao,recursosDemonstracao);
    }
    
    public void addDemonstracao(Demonstracao demonstracao){
        listaDemonstracoes.add(demonstracao);
    }
    @Override
    public String toString(){
        return "Lista de Demonstracoes: "+listaDemonstracoes;
    }
}
