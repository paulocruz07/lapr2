/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author 1150855_1150856
 */
@XmlRootElement
public class MecanismoCargaEquitativa implements MecanismoAtribuicao,Serializable{
    
    @Override
    /**
     * Metodo da superclasse MecanismoAtribuicao que adiciona mecanismo
     */
    public void addMecanismo(){
        RegistoMecanismos.addMecanismo(this.getClass().getSimpleName());
    }

    /**
     * Devolve lista de Atribuicoes 
     * @param e - Variavel do tipo Exposicao recebida por parametro
     * @return 
     */
    @Override
    public ArrayList<Atribuicao> getListaAtribuicoes(Exposicao e) {
       ArrayList<Atribuicao> la=new ArrayList();
       ListaCandidaturas rc= e.getListaCandidatura();
       ArrayList<Candidatura> lc=rc.getListaCandidatura();
       ListaFAE rfae=e.getListaFAE();
       ArrayList<FAE> lfae=rfae.getLfae();
       ListaAtribuicoes ra=e.getRegistoAtribuicao();
       
       int nrCandidaturas=lc.size();
       int nrFae=lfae.size();
       if(nrCandidaturas>=nrFae && nrFae!=0){
            int proporcao= (int)nrCandidaturas/nrFae;
            int i =0;
            int x=0;
            for(FAE fae:lfae){
                 for(i=x;i<nrCandidaturas;i++){
                     if(i==proporcao){
                             x=i;
                             proporcao+=proporcao;
                             break;
                     }
                         Atribuicao a=ra.newAtribuicao(lc.get(i), fae);
                         la.add(a);

                 }
            }
       }else{
            JOptionPane.showMessageDialog(null, "O número de candidaturas é inferior ao número de fae (Ou o nr de fae é igual a 0)!!");
       }
       return la;
    }
}
