/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ines Moura
 */
@XmlRootElement
public class AtribuicaoStands implements Serializable {
    private Exposicao e;
    private Stand s;



    public AtribuicaoStands(Exposicao exp,Stand s){
        this.e = exp;
        this.s = s;
    }

    /**
     *
     * @return
     */
    public Exposicao getExposicao(){
        return e;
    }

    /**
     *
     * @return
     */
    public Stand getStand(){
        return s;
    }

    public boolean valida() {
       return true;
    }
  
}
