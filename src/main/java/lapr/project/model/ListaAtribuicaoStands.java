/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jorge
 */
@XmlRootElement
public class ListaAtribuicaoStands implements Serializable{
    private ArrayList<AtribuicaoStands> listaAtribuicoesStand;

    public ListaAtribuicaoStands(ArrayList<AtribuicaoStands> lstAtribuicoesStand) {
        this.listaAtribuicoesStand = new ArrayList<>(lstAtribuicoesStand);
    }

    public ListaAtribuicaoStands() {
        this.listaAtribuicoesStand = new ArrayList();
    }

    /**
     * @return the listaAtribuicoesStand
     */
    public ArrayList<AtribuicaoStands> getListaAtribuicoesStand() {
        return listaAtribuicoesStand;
    }

    /**
     * @param listaAtribuicoesStand the listaAtribuicoesStand to set
     */
    public void setListaAtribuicoesStand(ArrayList<AtribuicaoStands> listaAtribuicoesStand) {
        this.listaAtribuicoesStand = listaAtribuicoesStand;
    }
    

    public void novaAtribuicao(Exposicao exp, Stand s) {
        AtribuicaoStands as = new AtribuicaoStands(exp,s);
        if(as.valida()){
            getListaAtribuicoesStand().add(as);
        } 
    }

  
}
