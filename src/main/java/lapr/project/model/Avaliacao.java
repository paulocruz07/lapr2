/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author 1150855_1150856
 */
@XmlRootElement
public class Avaliacao implements Serializable{
    /**
     * Variavel de instancia Candidatura candidatura
     */
    private final Candidatura candidatura;
    /**
     * Variavel de instancia FAE fae
     */
    private final FAE fae;
    /**
     * Variavel Boolean avaliada
     */
    private Boolean avaliada;
    /**
     * Variavel Boolean avaliacao
     */
    private Boolean avaliacao;
    /**
     * Variave String texto
     */
    private String texto;
    private int conhecimentoFAE;
    private int adequacaoCandidaturaExposicao;
    private int adequacaoCandidaturaDemonstracoes;
    private int adequacaoNrConvites;
    private int recomendacaoGlobal;
    
    

    /**
     * Contrutor com parametros Candidatura c e FAE fae
     * @param c - variavel do tipo Candidatura passada por parametro
     * @param fae - variavel do tipo FAE passada por parametro
     */
    public Avaliacao(Candidatura c, FAE fae)
    {
        this.candidatura=c;
        this.fae=fae;
        this.avaliada=false;
    }
    
    /**
     * Atualiza avaliacao
     * @param dec - variavel do tipo Boolean passada por parametro
     */
    public void setAvaliacao(Boolean dec) {
       this.avaliacao=dec;
    }  

    /**
     * Atualiza texto justificativo
     * @param textoJustificacao - variavel do tipo String passada por parametro 
     */
    public void setTextoDescricao(String textoJustificacao) {
       this.texto= textoJustificacao;
    }
    
    
     /**
     * Atualiza avaliada
     */ 
    public void setAvaliada(){
        this.avaliada = true;
    } 

    /**
     * @param conhecimentoFAE the conhecimentoFAE to set
     */
    public void setConhecimentoFAE(int conhecimentoFAE) {
        this.conhecimentoFAE = conhecimentoFAE;
    }

    /**
     * @param adequacaoCandidaturaExposicao the adequacaoCandidaturaExposicao to set
     */
    public void setAdequacaoCandidaturaExposicao(int adequacaoCandidaturaExposicao) {
        this.adequacaoCandidaturaExposicao = adequacaoCandidaturaExposicao;
    }

    /**
     * @param adequacaoCandidaturaDemonstracoes the adequacaoCandidaturaDemonstracoes to set
     */
    public void setAdequacaoCandidaturaDemonstracoes(int adequacaoCandidaturaDemonstracoes) {
        this.adequacaoCandidaturaDemonstracoes = adequacaoCandidaturaDemonstracoes;
    }

    /**
     * @param adequacaoNrConvites the adequacaoNrConvites to set
     */
    public void setAdequacaoNrConvites(int adequacaoNrConvites) {
        this.adequacaoNrConvites = adequacaoNrConvites;
    }

    /**
     * @param recomendacaoGlobal the recomendacaoGlobal to set
     */
    public void setRecomendacaoGlobal(int recomendacaoGlobal) {
        this.recomendacaoGlobal = recomendacaoGlobal;
    }
    
    /**
     * Retorna a informacao da Candidatura
     * @return 
     */
    public String getInfoCandidatura(){
        return this.candidatura.toString();
    }
    
   
}
