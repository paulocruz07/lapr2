/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jorge
 */
@XmlRootElement
public class ExposicaoEstadoConflitosAlterados extends SuperExposicao implements Serializable{
    
    Exposicao exp;

    public ExposicaoEstadoConflitosAlterados(Exposicao e) {
        this.exp = e;
    }

     @Override
    public boolean setCandidaturasAtribuidas() {
        if (valida()) {
            exp.setEstado(new ExposicaoEstadoCandidaturasAtribuidas(exp));
            return true;
        }
        return false;
    }



    @Override
    public boolean valida() {
        return true;
    }
}