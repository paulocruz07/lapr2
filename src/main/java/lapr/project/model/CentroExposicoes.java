/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Timer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimerTask;
import javax.swing.JOptionPane;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author lapr grupo 3
 */
@XmlRootElement
public class CentroExposicoes implements Agendavel,Serializable{
    /**
     * Variavel de instancia RegistoExposicoes listaExpo
     */
    private RegistoExposicoes listaExpo;
    /**
     * Variavel de instancia RegistoExposicoes listaExpo
     */
    private RegistoUtilizadores listaUtilizadores;

    /**
     * Variável de instancia RegistoStands registoStands
     */
    private RegistoStand registoStands;
    /**
     * Variavel de instancia RegistoMEcanismos listaMecanismos
     */
    private RegistoMecanismos listaMecanismos;
    
    
     private ArrayList<Timer> listaTimers;
     /**
     * Construtor com parametros
     * @param listaExpo - Variavel do tipo RegistoExposicoes recebida por parametro
     */
     
     private RegistoRecurso listaRecursos;
     
     /**
     * Construtor com parametros
     * @param listaExpo - Variavel do tipo RegistoExposicoes recebida por parametro
     * @param registoUtilizadores
     * @param registoStands
     * @param listaMecanismos
     * @param listaRecursos
     */
    public CentroExposicoes(RegistoExposicoes listaExpo,RegistoUtilizadores registoUtilizadores,RegistoStand registoStands, RegistoMecanismos listaMecanismos, RegistoRecurso listaRecursos){
        this.listaExpo=listaExpo;
        this.listaUtilizadores=registoUtilizadores;
        this.registoStands=registoStands;
        this.listaMecanismos=new RegistoMecanismos();
        this.listaRecursos=listaRecursos;
    }
    /**
     * Construtor sem parametros
     */
    public CentroExposicoes(){
        this.listaExpo=new RegistoExposicoes();
        this.listaUtilizadores=new RegistoUtilizadores();
        this.listaMecanismos=new RegistoMecanismos();
    }
    
    
     /**
     * @return the registoStands
     */
     
    public RegistoStand getRegistoStands() {
        return registoStands;
    }

    /**
     * @return the listaRecursos
     */
    
    public RegistoRecurso getListaRecursos() {
        return listaRecursos;
    }

    
    public RegistoExposicoes getRegistoExposicoes(){
        return listaExpo;
    }
    
    public RegistoUtilizadores getRegistoUtilizadores(){
        return listaUtilizadores;   
    }
     /**
     * Devolve o registo dos mecanismos
     * @return 
     */
    
    public RegistoMecanismos getRegistoMecanismos(){
        return listaMecanismos;
    }
    /**
     * @param listaExpo the listaExpo to set
     */
    public void setListaExpo(RegistoExposicoes listaExpo) {
        this.listaExpo = listaExpo;
    }
    
     /**
     * @param registoStands the registoStands to set
     */
    public void setRegistoStands(RegistoStand registoStands) {
        this.registoStands = registoStands;
    }
    
  
      public Exposicao novaExposicao(){
        RegistoExposicoes re=new RegistoExposicoes();
        return re.novaExposicao();
    }
     
    
    /**
     * @param listaMecanismos the listaMecanismos to set
     */
    public void setListaMecanismos(RegistoMecanismos listaMecanismos) {
        this.listaMecanismos = listaMecanismos;
    }
 
    /**
     * @param listaUtilizadores the listaUtilizadores to set
     */
    public void setListaUtilizadores(RegistoUtilizadores listaUtilizadores) {
        this.listaUtilizadores = listaUtilizadores;
    }
    
    
    /**
     * @param listaRecursos the listaRecursos to set
     */
    public void setListaRecursos(RegistoRecurso listaRecursos) {
        this.listaRecursos = listaRecursos;
    }
    
    
       /**
     * Devolve uma String com a lista das exposicoes
     * @return 
     */
    @Override
    public String toString(){
        return "exposicoes: "+getRegistoExposicoes();
    }

    @Override
    public void schedule(Object obj,String data){
        java.util.Timer t=new java.util.Timer();
        try{
            String str = data;
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date date = formatter.parse(str);
            t.schedule((TimerTask)obj, date);
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Erro a converter String para data");
        }
        listaTimers.add(t);
    }

    
    

    
}

