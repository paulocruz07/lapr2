/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lapr grupo 3 
 */
@XmlRootElement
public class Local implements Serializable{
    private String local;
    private static final String LOCAL_POR_OMISSAO="sem local";

    public Local(String local){
        this.local=local;
    }
    public Local(){
        this.local=LOCAL_POR_OMISSAO;
    }
    @Override
    public String toString(){
        return local;
    }
}
