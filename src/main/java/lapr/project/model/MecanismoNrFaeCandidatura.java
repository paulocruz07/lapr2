/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author 1150855_1150856
 */
@XmlRootElement
public class MecanismoNrFaeCandidatura implements MecanismoAtribuicao,Serializable{
    private Integer nrFaeCandidatura; 
    
    @Override
    /**
     * Metodo da superclasse MecanismoAtribuicao que adiciona mecanismo
     */
     public void addMecanismo(){
        RegistoMecanismos.addMecanismo(this.getClass().getSimpleName());
    }
      @Override
        /**
     * Devolve lista de Atribuicoes 
     * @param e - Variavel do tipo Exposicao recebida por parametro
     * @return 
     */
    public ArrayList<Atribuicao> getListaAtribuicoes(Exposicao e) {
       ArrayList<Atribuicao> la=new ArrayList();
       ListaCandidaturas rc= e.getListaCandidatura();
       ArrayList<Candidatura> lc=rc.getListaCandidatura();
       ListaFAE rfae=e.getListaFAE();
       ArrayList<FAE> lfae=rfae.getLfae();
       ListaAtribuicoes ra=e.getRegistoAtribuicao();
       
       int nrFae=lfae.size();
       if(nrFaeCandidatura<=nrFae){
            int i =0;
            for(Candidatura c:lc){
                 for(i=0;i<nrFaeCandidatura;i++){
                     Atribuicao a=ra.newAtribuicao(c, lfae.get(i));
                     la.add(a);
                 }
            }
       }else{
            JOptionPane.showMessageDialog(null, "O número de fae's existentes é inferior ao número de fae's pretendidos!!");
       }
       
       return la;
    }

    /**
     * @param nrFaeCandidatura the nrFaeCandidatura to set
     */
    public void setNrFaeCandidatura(Integer nrFaeCandidatura) {
        this.nrFaeCandidatura = nrFaeCandidatura;
    }
    
}
