/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author 1150855_1150856
 */
@XmlRootElement
public class Produto implements Serializable{
    /**
     * Variavel de instancia String nome
     */
    private String nome;
    
    /**
     * Construtor com parametros
     * @param nome - Variavel do tipo Utilizador recebida por parametro 
     */
    public Produto(String nome){
        this.nome=nome;
    }
}
