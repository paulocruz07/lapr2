/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lapr grupo 3
 */
@XmlRootElement
public interface ExposicaoEstado extends Serializable{

    public boolean valida();

    public boolean setCriada();

    public boolean setFaeDefinido();

    public boolean setDemonstracaoDefinida();
    
    public boolean setCandidaturaAberta();
    
    public boolean setCandidaturaTerminada();
    
    public boolean setConflitosDetetados();
    
    public boolean setConflitosAlterados();
    
    public boolean setCandidaturasAtribuidas();
    
    public boolean setCandidaturasAvaliadas();
    
    public boolean setCandidaturasDecididas();
    
    public boolean setTerminada();
    
}
