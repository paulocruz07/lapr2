/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author 1150855_1150856
 */
@XmlRootElement
public class MecanismoNivelExperiencia implements MecanismoAtribuicao,Serializable{
    
     @Override
     /**
     * Metodo da superclasse MecanismoAtribuicao que adiciona mecanismo
     */
     public void addMecanismo(){
        RegistoMecanismos.addMecanismo(this.getClass().getSimpleName());
    }

      /**
     * Devolve lista de Atribuicoes 
     * @param e - Variavel do tipo Exposicao recebida por parametro
     * @return 
     */
    @Override
    public ArrayList<Atribuicao> getListaAtribuicoes(Exposicao e) {
       ArrayList<Atribuicao> la=new ArrayList();
       ListaCandidaturas rc= e.getListaCandidatura();
       ArrayList<Candidatura> lc=rc.getListaCandidatura();
       ListaFAE rfae=e.getListaFAE();
       ArrayList<FAE> lfae=rfae.getLfae();
       ListaAtribuicoes ra=e.getRegistoAtribuicao();
       
       if(!lc.isEmpty()){
            int maior=lfae.get(0).getNivelExperiencia();
            FAE maiorFAE=lfae.get(0);
            for(FAE fae:lfae){
                int nivel=fae.getNivelExperiencia();
                if(nivel>maior){
                    maior=nivel;
                    maiorFAE=fae;
                }
            }
            for(Candidatura c:lc){
                Atribuicao a=ra.newAtribuicao(c, maiorFAE);
                la.add(a);
            }
       }else{
           JOptionPane.showMessageDialog(null, "Não existem candidaturas");
       }
       return la;
    }
}
