/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jorge
 */
@XmlRootElement
public class ExposicaoEstadoCandidaturaTerminada extends SuperExposicao implements Serializable{
    
    Exposicao exp;

    public ExposicaoEstadoCandidaturaTerminada(Exposicao e) {
        this.exp = e;
    }

     @Override
    public boolean setConflitosDetetados() {
        if (valida()) {
            exp.setEstado(new ExposicaoEstadoConflitosDetetados(exp));
            return true;
        }
        return false;
    }



    @Override
    public boolean valida() {
        return true;
    }
}