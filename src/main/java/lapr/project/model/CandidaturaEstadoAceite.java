/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jorge
 */
@XmlRootElement
public class CandidaturaEstadoAceite extends SuperCandidatura implements Serializable {
    
    Candidatura cand;
    
    public CandidaturaEstadoAceite(Candidatura cand){
        this.cand = cand;
    }
    
    @Override
    public boolean setStandsAtribuidos(){
        if(valida()){
            cand.setEstado(new CandidaturaEstadoStandsAtribuidos(cand));
            return true;
        }
        return false;
    }
       
    
    
    @Override
    public boolean valida(){
        return true;
    }
}
