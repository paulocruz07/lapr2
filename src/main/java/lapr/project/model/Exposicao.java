/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lapr grupo 3
 */
@XmlRootElement
public class Exposicao implements Serializable{
    
    private SuperExposicao estado;
    private String titulo;
    private String descricao;
    private String dataInicio;
    private String dataFim;
    private String subInicio;
    private String subFim;
    private Local local;
    private String dataLimiteConflitos;
    private ListaOrganizadores listaOrganizadores = new ListaOrganizadores();
    private ListaFAE listaFAE;
    private ListaAtribuicoes listaAtribuicoes;
    private ListaCandidaturas listaCandidatura;
    private RegistoStand listaStands;
    private ListaAtribuicaoStands las;
    private static final String TITULO_POR_OMISSAO="sem titulo";
    private static final String DESCRICAO_POR_OMISSAO="sem descricao";
    private static final String DATAINICIO_POR_OMISSAO="11/06/2016";
    private static final String DATAFIM_POR_OMISSAO="12/06/2016";
    private static final String SUBINICIO_POR_OMISSAO="13/06/2016";
    private static final String SUBFIM_POR_OMISSAO="14/06/2016";
    private static final String DATACONFLITOS_POR_OMISSAO="20/06/2016";

    
    public Exposicao(String titulo,String descricao,String dataInicio,String dataFim,String subInicio,String subFim,String local,String dataLimiteConflitos){
        this.titulo=titulo;
        this.descricao=descricao;
        this.dataInicio=dataInicio;
        this.dataFim=dataFim;
        this.subInicio=subInicio;
        this.subFim=subFim;
        this.local=new Local(local);
        this.dataLimiteConflitos=dataLimiteConflitos;
        this.listaOrganizadores=new ListaOrganizadores();
        this.listaFAE=new ListaFAE();
        this.listaAtribuicoes=new ListaAtribuicoes();
        this.listaCandidatura=new ListaCandidaturas();
        this.listaStands=new RegistoStand();
        this.las=new ListaAtribuicaoStands();
        
        

    }
    
    public Exposicao(){
        this.titulo=TITULO_POR_OMISSAO;
         this.descricao=DESCRICAO_POR_OMISSAO;
        this.dataInicio=DATAINICIO_POR_OMISSAO;
        this.dataFim=DATAFIM_POR_OMISSAO;
        this.subInicio=SUBINICIO_POR_OMISSAO;
        this.subFim=SUBFIM_POR_OMISSAO;
        this.local=new Local();
        this.dataLimiteConflitos=DATACONFLITOS_POR_OMISSAO;
        this.listaOrganizadores=new ListaOrganizadores();
        this.listaFAE=new ListaFAE();
        this.listaAtribuicoes=new ListaAtribuicoes();
        this.listaCandidatura=new ListaCandidaturas();
        this.listaStands=new RegistoStand();
        this.las=new ListaAtribuicaoStands();
        
    }

    /**
     * @return the listaStands
     */
    public RegistoStand getListaStands() {
        return listaStands;
    }
    
    public SuperExposicao getEstado(){
        return estado;
    }
    public ListaAtribuicoes getRegistoAtribuicao(){
        return listaAtribuicoes;
    }
    
    /**
     * Devolve FAE
     * @param fae - Variavel do tipo FAE recebida por parametro
     * @return 
     */
    public FAE getFAE(FAE fae){
        return listaFAE.getFAE(fae);
    }
    
    public Organizador getOrganizador(Organizador o){
        return getListaOrganizadores().getOrganizador(o);
    }
     /**
     * Devolve uma String/informacao da candidatura por avaliar
     * @param c - Variavel do tipo Candidatura recebida por parametro 
     * @return 
     */

    public String getInformacaoDaCandidaturaPorAvaliar(Candidatura c){
        return c.toString();
    }
    
    /**
     * Devolve Lista de CAndidaturas
     * @return the listaCandidatura
     */
 
    public ListaCandidaturas getListaCandidatura() {
        return listaCandidatura;
    }
    
    

    public ListaOrganizadores getListaOrganizadores() {
        return listaOrganizadores;
    }

    public ListaFAE getListaFAE() {
        return listaFAE;
    }

    /**
     * @return the titulo
     */
   
    public String getTitulo() {
        return titulo;
    }

    /**
     * @return the descricao
     */
    
    public String getDescricao() {
        return descricao;
    }

    /**
     * @return the dataInicio
     */
    
    public String getDataInicio() {
        return dataInicio;
    }

    /**
     * @return the dataFim
     */
    
    public String getDataFim() {
        return dataFim;
    }

    /**
     * @return the subInicio
     */
    
    public String getSubInicio() {
        return subInicio;
    }

    /**
     * @return the subFim
     */
    public String getSubFim() {
        return subFim;
    }

    /**
     * @return the local
     */

    public Local getLocal() {
        return local;
    }

    /**
     * @return the dataLimiteConflitos
     */

    public String getDataLimiteConflitos() {
        return dataLimiteConflitos;
    }
    
     /**
     * Devolve lista de candidaturas por avaliar
     * @param fae -  Variavel do tipo FAE recebida por parametro
     * @return 
     */

    public ArrayList<Candidatura> getListaCandidaturaPorAvaliar(FAE fae){
        ArrayList<Atribuicao> lc = listaAtribuicoes.getListaAtribuicoes();
        ArrayList<Candidatura> lista= new ArrayList<>();
        for(Atribuicao a:lc){
            Candidatura c=a.getC();
            if(c.getAvaliacao()==null&& a.getFae()==fae){
                lista.add(c);
            }
             
        }
       
        return lista;
    }
    
 
    public ListaCandidaturas getRegistoCandidaturas() {
        return listaCandidatura;
    }

    /**
     * @param titulo the titulo to set
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @param lf 
     */
    public void setListaFAE(ListaFAE lf){
        this.listaFAE = lf;
    }
    
    /**
     * @param listaAtribuicoes the listaAtribuicoes to set
     */
    public void setListaAtribuicoes(ListaAtribuicoes listaAtribuicoes) {
        this.listaAtribuicoes = listaAtribuicoes;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * @param dataInicio the dataInicio to set
     */
    public void setDataInicio(String dataInicio) {
        this.dataInicio = dataInicio;
    }
    
     /**
     * Atualiza lista de organizadores
     * @param listaOrganizadores - Variavel do tipo ListaOrganizadores recebida por parametro 
     */
    public void setlistaOrganizadores(ListaOrganizadores listaOrganizadores){
        this.listaOrganizadores=listaOrganizadores;
    }

    /**
     * @param dataFim the dataFim to set
     */
    public void setDataFim(String dataFim) {
        this.dataFim = dataFim;
    }

    /**
     * @param subInicio the subInicio to set
     */
    public void setSubInicio(String subInicio) {
        this.subInicio = subInicio;
    }

    /**
     * @param subFim the subFim to set
     */
    public void setSubFim(String subFim) {
        this.subFim = subFim;
    }

    /**
     * @param local the local to set
     */
    public void setLocal(Local local) {
        this.local = local;
    }

    /**
     * @param dataLimiteConflitos the dataLimiteConflitos to set
     */
    public void setDataLimiteConflitos(String dataLimiteConflitos) {
        this.dataLimiteConflitos = dataLimiteConflitos;
    }

    public void setEstado(SuperExposicao estado2) {
        this.estado = estado2;
    }

    public void novaAtribuicao(Exposicao exp, Stand s){
       las.novaAtribuicao(exp,s);
    }
    public void setCriada(){
         ExposicaoEstadoInicial expInicial=new ExposicaoEstadoInicial(this);
         expInicial.setCriada();
    }
    
 

    public void registaDados() {
       addLista(las);
    }

    private void addLista(ListaAtribuicaoStands las) {
       this.las = las;
    }
    
   public boolean setFaeDefinido(){
       if(estado.setFaeDefinido()){
           return true;
       }
       return false;
       
   }
    /**
     * Adiciona candidatura
     * @param c - Variavel do tipo Candidatura recebida por parametro
     */
    public void adicionaCandidatura(Candidatura c){
        listaCandidatura.add(c);
    }
    
    public void adicionaStand(Stand s){
        getListaStands().addStand(s);
    }
    
   
    public boolean isValidForCriada(){
        ExposicaoEstadoInicial expInicial=new ExposicaoEstadoInicial(this);
        return expInicial.isValidaForCriada();
    }
    
    /**
     * Devolve uma String com informacao de exposicao
     * @return 
     */
    @Override
    public String toString(){
        return ""+titulo+","+descricao;
    }


}
