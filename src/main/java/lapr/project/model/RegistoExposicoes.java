/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lapr grupo 3
 */
@XmlRootElement
public class RegistoExposicoes implements Serializable{

    private final ArrayList<Exposicao> listaExposicoes;

    public RegistoExposicoes(ArrayList<Exposicao> listaExposicoes) {
        this.listaExposicoes = listaExposicoes;
    }

    public RegistoExposicoes() {
        this.listaExposicoes = new ArrayList();
    }

    /**
     * @return the listaExposicoes
     */
    
    public ArrayList<Exposicao> getListaExposicoes() {
        return listaExposicoes;
    }

    public Exposicao novaExposicao() {
        Exposicao e = new Exposicao();
        e.setEstado(new ExposicaoEstadoInicial(e));
        return e;
    }

    public boolean validaExposicao1(Exposicao e) {
        return e.isValidForCriada();
    }

    private boolean validaExposicao2(Exposicao e) {
        if(listaExposicoes.contains(e)){
            return false;
        }
        return true;
    }

    public boolean registaExposicao(Exposicao e) {
        if(validaExposicao2(e)){
            e.setCriada();
            return add(e);
        }
        return false;
    }

    
    /**
     * Devolve lista de Exposicoes referentes ao FAE
     * @param fae - Variavel do tipo FAE recebida por parametro
     * @return 
     */
    public ArrayList<Exposicao> getListaExposicoesDoFAE(FAE fae){
        
        ArrayList <Exposicao> listaExposicoesDoFAE=new ArrayList();
        
        for (Exposicao m: this.getListaExposicoes())
        {
            if(m.getFAE(fae)!=null)
                listaExposicoesDoFAE.add(m);
                      
        }
        return listaExposicoesDoFAE;
    }
    
    /**
     * Devolve lista de Exposicoes referentes ao Organizador
     * @param o
     * @return 
     */
    public ArrayList<Exposicao> getListaExposicoesDoOrganizador(Organizador o){
        
        ArrayList <Exposicao> listaExposicoesDoOrganizador=new ArrayList();
        
        for (Exposicao m: this.getListaExposicoes())
        {
            if(m.getOrganizador(o)!=null)
                listaExposicoesDoOrganizador.add(m);
                      
        }
        return listaExposicoesDoOrganizador;
    }
     public ArrayList<Exposicao> getListaExposicoesDoOrganizadorNaoAtribuidas(Organizador o){
        
        ArrayList <Exposicao> listaExposicoesDoOrganizador=new ArrayList();
        
        for (Exposicao m: this.getListaExposicoes())
        {
            if(m.getOrganizador(o)!=null && !(m.getEstado() instanceof ExposicaoEstadoCandidaturasAtribuidas))
                listaExposicoesDoOrganizador.add(m);
                      
        }
        return listaExposicoesDoOrganizador;
    }

    
    public ArrayList<Exposicao> getExposicoesSemFAE(Organizador o) {
        ArrayList<Exposicao> lsemFAE = new ArrayList();
        for (Exposicao e : getListaExposicoes()) {
            if (e.getEstado() instanceof ExposicaoEstadoCriada) {
                lsemFAE.add(e);
            } else if (e.getEstado() instanceof ExposicaoEstadoDemonstracaoSemFAEDefinido) {
                lsemFAE.add(e);
            }
        }
        return lsemFAE;
    }

    
    /**
     * Adiciona exposicao
     * @param e - Variavel do tipo Exposicao recebida por parametro
     * @return 
     */
    public boolean addExposicao(Exposicao e){
        return add(e);
    }

    private boolean add(Exposicao e) {
        return listaExposicoes.add(e);
    }

    public void getListaExposicoesDecididas(Organizador o) {
        //Por implementar
    }
    public String toString(){
        return "Lista das exposicoes: "+listaExposicoes;
    }
}
