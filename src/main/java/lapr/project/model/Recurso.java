/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lapr grupo 3
 */
@XmlRootElement

public class Recurso implements Serializable{
    
    private String descricao;
    
    public Recurso(String descricao){
        this.descricao=descricao;
    }
    
    public Recurso(){   
        
    }

    /**
     * @return the descricao
     */
    
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
    @Override
    public String toString(){
        return "Descricao: "+descricao;
    }
    
    public boolean valida(){
        
    if(descricao == null || descricao.equals(""))
        return false;

    if(!descricao.matches("[a-zA-Z]*"))
        return false;

    return true;
           
    }
    
}
