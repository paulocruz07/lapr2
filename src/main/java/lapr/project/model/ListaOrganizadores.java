/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lapr grupo 3
 */
@XmlRootElement
public class ListaOrganizadores implements Serializable{
    private ArrayList<Organizador> listaOrganizadores;
    
    public ListaOrganizadores(ArrayList<Organizador> lstOrganizador){
        this.listaOrganizadores=new ArrayList<>(lstOrganizador);
    }
    
    public ListaOrganizadores(){
        this.listaOrganizadores=new ArrayList();
    }

    /**
     * @return the listaOrganizadores
     */
    public ArrayList<Organizador> getListaOrganizadores() {
        return listaOrganizadores;
    }
    
    
     public Organizador getOrganizador(Organizador o) {
        return getListaOrganizadores().get(getListaOrganizadores().indexOf(o));
    }

    /**
     * @param listaOrganizadores the listaOrganizadores to set
     */
    public void setListaOrganizadores(ArrayList<Organizador> listaOrganizadores) {
        this.listaOrganizadores = listaOrganizadores;
    }
     
    public void addOrganizador(Organizador o){
        validaOrganizador(o);
        add(o);
    }
    private boolean validaOrganizador(Organizador o){
        return true;
    }
    private void add(Organizador o){
        getListaOrganizadores().add(o);
    }
}
