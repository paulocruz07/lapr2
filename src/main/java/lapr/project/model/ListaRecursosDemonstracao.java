/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author tiagoemanuelgomesnov
 */
@XmlRootElement
public class ListaRecursosDemonstracao implements Serializable{
    
    private ArrayList<Recurso> recursosDemonstracao;
    private Recurso recurso;
    
    public ListaRecursosDemonstracao(ArrayList<Recurso> recursosDemonstracao){
        this.recursosDemonstracao=recursosDemonstracao;
    }
    
    public ListaRecursosDemonstracao(){
        
    }

    /**
     * @return the recursosDemonstracao
     */
    public ArrayList<Recurso> getRecursosDemonstracao() {
        return recursosDemonstracao;
    }

    /**
     * @param recursosDemonstracao the recursosDemonstracao to set
     */
    public void setRecursosDemonstracao(ArrayList<Recurso> recursosDemonstracao) {
        this.recursosDemonstracao = recursosDemonstracao;
    }
    
    
    public void addRecursoDemonstracao(){
        recursosDemonstracao.add(recurso);
    }
    
    
    
    
}
