/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author 1150855_1150856
 */
@XmlRootElement
public class ListaAtribuicoes implements Serializable{
    /**
     * Variavel de instancia ArrayList listaAtribuicoes
     */
    private ArrayList<Atribuicao> listaAtribuicoes;

    /**
     * Construtor com parametros
     * @param lst - Variavel do tipo ArrayList recebida por parametro
     */
    public ListaAtribuicoes(ArrayList<Atribuicao> lst) {
        listaAtribuicoes = lst;
    }
    /**
     * Construtor sem parametros
     */
    public ListaAtribuicoes() {
        listaAtribuicoes = new ArrayList<>();
    }
    
     /**
      * Devolve lista de atribuicoes
     * @return the listaAtribuicoes
     */
    public ArrayList<Atribuicao> getListaAtribuicoes() {
        return listaAtribuicoes;
    }
    
    /**
     * Devolve Atribuicao
     * @param c - Variavel do tipo Candidatura recebida por parametro
     * @param fae - Variavel do tipo FAE recebida por parametro
     * @return 
     */
    public Atribuicao newAtribuicao(Candidatura c,FAE fae){
        Atribuicao a = new Atribuicao(fae,c);
        return a;
    }
    
    /**
     * Regista atribuicoes feitas e devolve true
     * @param listaAtribuicoes - Variavel do tipo ArrayList recebida por parametro
     * @return 
     */
    public boolean registarAtribuicoes(ArrayList<Atribuicao> listaAtribuicoes){
        this.listaAtribuicoes = listaAtribuicoes;
        return true;
    }

   
    
}
