/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jorge
 */
@XmlRootElement
public class CandidaturaEstadoEmAvaliacao extends SuperCandidatura implements Serializable {

    Candidatura cand;
    
    public CandidaturaEstadoEmAvaliacao(Candidatura cand){
        this.cand = cand;
    }
    
    @Override
    public boolean setNaoAvaliada(){
        if(valida()){
            cand.setEstado(new CandidaturaEstadoNaoAvaliada(cand));
            return true;
        }
        return false;
    }
    
    @Override
    public boolean setAvaliada(){
        if(valida()){
            cand.setEstado(new CandidaturaEstadoAvaliada(cand));
            return true;
        }
        return false;
    }
    
    
    
    @Override
    public boolean valida(){
        return true;
    }
}
