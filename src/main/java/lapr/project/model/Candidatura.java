/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author 1150855_1150856
 */
@XmlRootElement
public class Candidatura implements Serializable {

    /**
     * Variavel de instancia int qtd_convites
     */
    private int qtd_convites;
    /**
     * Variavel de instancia Avaliacao av
     */
    private Avaliacao av;
    /**
     * Variavel de instancia String areaPretendida
     */
    private String areaPretendida;
    /**
     * Variavel de instancia String nome
     */
    private String nome;
    /**
     * Variavel de instancia String morada
     */
    private String morada;
    /**
     * Variavel de instancia int telemovel
     */
    private int telemovel;
    /**
     * Variavel de instancia ArrayList listaProduto
     */
    private ArrayList<Produto> listaProduto;

    private SuperCandidatura estado;

    private ArrayList<Keyword> listaKeywords;

    /**
     * Contrutor com parametros
     *
     * @param nome - Variavel do tipo String recebida por parametro
     * @param morada - Variavel do tipo String recebida por parametro
     * @param telemovel - Variavel do tipo Inteiro recebida por parametro
     * @param qtd_convites - Variavel do tipo Inteiro recebida por parametro
     * @param areaPretendida - Variavel do tipo String recebida por parametro
     */
    public Candidatura(String nome, String morada, int telemovel, int qtd_convites, String areaPretendida) {
        this.qtd_convites = qtd_convites;
        this.areaPretendida = areaPretendida;
        this.nome = nome;
        this.morada = morada;
        this.telemovel = telemovel;
        listaProduto = new ArrayList<>();
        listaKeywords = new ArrayList();
    }

    /**
     * Retorna a quantidade de convites
     *
     * @return the qtd_convites
     */
    public int getQtd_convites() {
        return qtd_convites;
    }

    /**
     * Retorna area pretendida
     *
     * @return the areaPretendida
     */
    public String getAreaPretendida() {
        return areaPretendida;
    }

    public ArrayList<Produto> getListaProdutos(){
        return listaProduto;
    }
    /**
     * Retorna avaliacao
     *
     * @return
     */
    public Avaliacao getAvaliacao() {
        return av;
    }
     public ArrayList<Keyword> getListaKeywords() {
        return listaKeywords;
    }

    /**
     * Atualiza quantidade de convites
     *
     * @param qtd_convites - Variavel do tipo String recebida por parametro
     */
    public void setQtd_convites(int qtd_convites) {
        this.qtd_convites = qtd_convites;
    }

    /**
     * Atualiza area pretendida
     *
     * @param areaPretendida - Variavel do tipo String recebida por parametro
     */
    public void setAreaPretendida(String areaPretendida) {
        this.areaPretendida = areaPretendida;
    }

    public void setEstado(SuperCandidatura estado2) {
        this.estado = estado2;
    }

    /**
     * Retorna avaliacao
     *
     * @param candidatura- Variavel do tipo Candidatura recebida por parametro
     * @param fae- Variavel do tipo FAE recebida por parametro
     * @return
     */
    public Avaliacao novaAvaliacao(Candidatura candidatura, FAE fae) {
        return av = new Avaliacao(candidatura, fae);
    }

    /**
     * Adiciona produto à lista
     *
     * @param produto - Variavel do tipo Produto recebida por parametro
     */
    public void addProduto(Produto produto) {
        listaProduto.add(produto);
    }

    public void addKeywords(Keyword keyword) {
        listaKeywords.add(keyword);
    }

    /**
     * Retorna uma String com informacao da Candidatura
     *
     * @return
     */
    @Override
    public String toString() {
        return "Nome " + nome + ", morada " + morada + ", telemovel " + telemovel + ", quantidade de convites " + qtd_convites + ", area pretendida " + areaPretendida;
    }

}
