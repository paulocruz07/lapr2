/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lapr grupo 3
 */
@XmlRootElement
public class Stand implements Serializable{
    
    /**
     * Variável de instância nome (Nome do Stand)
     */
    
    private String nome;
    
    /**
     * Variável de instância area (área do Stand)
     */
    private double area;
    
    /**
     * Construtor da classe Stand (nome e area)
     * @param nome
     * @param area 
     */
    
    public Stand (String nome, double area){
        this.nome=nome;
        this.area=area;
    }
    
    /**
     * Construtor vazio da classe Stand
     */
    
    public Stand(){   
    }

    /**
     * @return the nome
     */
    
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the area
     */
    
    public double getArea() {
        return area;
    }

    /**
     * @param area the area to set
     */
    
    public void setArea(double area) {
        this.area = area;
    }
    /**
     * Método que retorna frase informativa sobre as variáveis de instância da classe
     * @return 
     */
    
    @Override
    public String toString(){
        return "Nome Stand: "+nome+", Area: "+area+".";
    }
    /**
     * Método boleano de validação
     * @return 
     */
    public boolean valida(){
        
    if(nome == null || nome.equals(""))
        return false;

    if(!nome.matches("^(?=.*[A-Z])(?=.*[0-9])[A-Z0-9]+$"))
        return false;
    
//UI TRY CATCH . QUANDO GET TEXT, SHOW MESSAGE DIALOG, NUM INTRODUZIDO MUST BE DOUBLE
    
    return true;
    }
            
}
