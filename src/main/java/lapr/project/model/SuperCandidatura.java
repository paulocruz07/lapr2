/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jorge
 */
@XmlRootElement
public class SuperCandidatura implements CandidaturaEstado,Serializable {
    @Override
    public boolean valida(){
        return false;
    }
    
    @Override
    public boolean setEmSubmissao(){
        return false;
    }
    
    @Override
    public boolean setEmAvaliacao(){
        return false;
    }
    
    @Override
    public boolean setRemovida(){
        return false;
    }
    
    @Override
    public boolean setNaoAvaliada(){
        return false;
    }
    
    @Override
    public boolean setAvaliada(){
        return false;
    }
    
    @Override
    public boolean setTerminada(){
        return false;
    }
    
    @Override
    public boolean setRejeitada(){
        return false;
    }
    
    @Override
    public boolean setAceite(){
        return false;
    }
    
    @Override
    public boolean setStandsAtribuidos(){
        return false;
    }
}
