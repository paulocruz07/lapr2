/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jorge
 */
@XmlRootElement
public class CandidaturaEstadoAvaliada  extends SuperCandidatura implements Serializable {
    Candidatura cand;
    
    public CandidaturaEstadoAvaliada(Candidatura cand){
        this.cand = cand;
    }
    
    @Override
    public boolean setAceite(){
        if(valida()){
            cand.setEstado(new CandidaturaEstadoAceite(cand));
            return true;
        }
        return false;
    }
    
        @Override
    public boolean setRejeitada(){
        if(valida()){
            cand.setEstado(new CandidaturaEstadoRejeitada(cand));
            return true;
        }
        return false;
    }
    
    
    
    @Override
    public boolean valida(){
        return true;
    }
}


