/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.io.Serializable;
import java.util.ArrayList;
import lapr.project.model.*;




public class ConfirmarRegistoUtilizadorController implements Serializable{
    
    private CentroExposicoes ce;
    private RegistoUtilizadores ru;
    private Utilizador u;
    
    public ConfirmarRegistoUtilizadorController(CentroExposicoes ce){
        this.ce=ce;
        this.ru=ce.getRegistoUtilizadores();
    }

    /**
     * @return the ce
     */
    public CentroExposicoes getCe() {
        return ce;
    }

    /**
     * @param ce the ce to set
     */
    public void setCe(CentroExposicoes ce) {
        this.ce = ce;
    }
    
    
    public ArrayList<Utilizador> getUtilizadores(){
       return ru.getListaUtilizadores();
    }
    
    
    public String getRegistoUtilizadores(Utilizador u){
        this.u=u;
        return u.toString();
    }
    
    
    public void confirmaRegisto(Boolean confirmacao){
            u.setConfirmado();
            u.setRegistado(confirmacao);
        }
    }
    

