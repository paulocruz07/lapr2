/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.io.Serializable;
import java.util.ArrayList;
import lapr.project.model.*;

/**
 *
 * @author 1150855_1150856
 */
public class RegistaCandidaturaController implements Serializable{
    /**
     * Variavel de instancia CentroExposicoes ce
     */
    private CentroExposicoes ce;
    /**
     * Variavel de instancia Exposicao e
     */
    private Exposicao e;
    
    /**
     * Contrutor com parametro do tipo CentroExposicoes 
     * @param ce - variavel do tipo CentroExposicoes passada por parametro
     */
    public RegistaCandidaturaController(CentroExposicoes ce){
        this.ce=ce;
        this.e=ce.getRegistoExposicoes().getListaExposicoes().get(0);
    }
    /**
     * Retorna uma lista de Exposicoes
     * @return 
     */
    public ArrayList<Exposicao> getListaExposicoes() {
        RegistoExposicoes re= ce.getRegistoExposicoes();
        return re.getListaExposicoes();
    }
    /**
     * Retorna Exposicao
     * @return 
     */
    public Exposicao getExposicao(){
        return e;
    }
    /**
     * Atualiza Exposicao
     * @param exposicao 
     */
     public void setExposicao(Exposicao exposicao){
         this.e=exposicao;
     }
     
}
