/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import lapr.project.model.*;

/**
 *
 * @author 1150855_1150856
 */
public class AvaliarCandidaturasController implements Serializable{
    
    /**
     * Variavel de instancia CentroExposicoes ce
     */
    private final CentroExposicoes ce;
    /**
     * Variavel de instancia Exposicao exposicao
     */
    private Exposicao exposicao;
    /**
     * Variavel de instancia Avaliacao avaliacao
     */
    private Avaliacao avaliacao;
    /**
     * Variavel de instancia FAE fae
     */
    private FAE fae;
    /**
     * Variavel de instancia listaFAE lfae
     */
    private ListaFAE lfae;
    /**
     * Variavel de instancia Atribuicao atribuicao
     */
    private Atribuicao atribuicao;
    /**
     * Variavel de instancia Candidatura candidatura
     */
    private Candidatura candidatura;
    
    
    
  
    /**
     * Contrutor com parmetro CentroExposicoes ce
     * @param ce - variavel do tipo CentroExposicoes passada por parametro
     */
    public AvaliarCandidaturasController(CentroExposicoes ce) {
        this.ce = ce;
        this.exposicao=ce.getRegistoExposicoes().getListaExposicoes().get(0);
        this.lfae=exposicao.getListaFAE();
   }
    
    /**
     * Retorna lista de FAE
     * @return 
     */
     public ArrayList<FAE> getListaFAE(){
        ArrayList<FAE>lFAE=new ArrayList();
        ArrayList<Exposicao>le=ce.getRegistoExposicoes().getListaExposicoes();
        for(Exposicao e :le ){
            ArrayList<FAE>lFAEcopia=e.getListaFAE().getLfae();
            for(FAE fae:lFAEcopia){
                lFAE.add(fae);
            }
        }
        return lFAE;
    }
    
     /**
      * Retorna lista de Exposicoes associadas ao FAE
      * @param fae - variavel do tipo FAE passada por parametro
      * @return 
      */
    public ArrayList<Exposicao> getListaExposicoesDoFAE(FAE fae) {
        RegistoExposicoes re= ce.getRegistoExposicoes();
        return re.getListaExposicoesDoFAE(fae);
    }

    /**
     * Retorna a lista de Candidaturas por avaliar
     * @param exposicao - variavel do tipo Exposicoes passada por parametro 
     * @param fae - variavel do tipo FAE passada por parametro
     * @return 
     */
    public ArrayList<Candidatura> selecionaExposicao(Exposicao exposicao, FAE fae) {
        this.exposicao=exposicao;
        this.fae=exposicao.getFAE(fae); 
        return exposicao.getListaCandidaturaPorAvaliar(fae);  
    }

    /**
     * Retorna a informacao referente à candidatura por avaliar
     * @param c - variavel do tipo Candidatura passada por parametro
     * @return 
     */
    public String getInformacaoDaCandidaturaPorAvaliar(Candidatura c) {
        this.candidatura=c;
        return this.exposicao.getInformacaoDaCandidaturaPorAvaliar(c); //falta implementar os metodos nas classes
    }
      
    /**
     * Atualiza avaliacao e texto justificativo
     * @param dec - variavel do tipo Boolean passada por parametro
     * @param textoJustificacao - variavel do tipo String passada por parametro
     */
    public void setAvaliacao(Boolean dec, String textoJustificacao,int x1,int x2,int x3,int x4,int x5) {
        
        avaliacao=novaAvaliacao();
        avaliacao.setAvaliacao(dec);
        avaliacao.setTextoDescricao(textoJustificacao);
        avaliacao.setConhecimentoFAE(x1);
        avaliacao.setAdequacaoCandidaturaExposicao(x2);
        avaliacao.setAdequacaoCandidaturaDemonstracoes(x3);
        avaliacao.setAdequacaoNrConvites(x4);
        avaliacao.setRecomendacaoGlobal(x5);
//        candidatura.validaAvaliacao(avaliacao);
    }

    /**
     * Retorna Avaliacao
     * @return 
     */
    public Avaliacao novaAvaliacao(){
        return candidatura.novaAvaliacao(candidatura,fae);
    }
    
   
    /**
     * Retorna uma lista de atribuicoes por avaliar
     * @param fae - variavel do tipo FAE passada por parametro
     * @return 
     */
    public ArrayList<Candidatura> getListaAtribuicoesPorAvaliar(FAE fae){
        return exposicao.getListaCandidaturaPorAvaliar(fae);
    }
    
    /**
     * Retorna exposicao
     * @return 
     */
    public Exposicao getExposicao(){
        return exposicao;
    }
    
    /**
     * Atualiza candidatura
     * @param c 
     */
    public void setCandidatura(Candidatura c){
        this.candidatura=c;
    }
    
    /**
     * Atualiza de Exposicao
     * @param expo - variavel do tipo Exposicao passada por parametro
     */
    public void setExposicao(Exposicao expo){
        this.exposicao=expo;
        this.lfae=exposicao.getListaFAE();
    }
}
