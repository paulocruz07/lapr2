/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.io.Serializable;
import lapr.project.model.Demonstracao;
import lapr.project.model.ListaRecursosDemonstracao;

/**
 *
 * @author tiagoemanuelgomesnov
 */
public class CriarDemonstracaoController implements Serializable{
    
    private Demonstracao demonstracao;
    
    
    public void novaDemonstracao(String codigo, String descricao, ListaRecursosDemonstracao recursosDemonstracao){
       demonstracao.setCodigo(codigo);
       demonstracao.setDescricao(descricao);
       demonstracao.setRecursosDemonstracao(recursosDemonstracao);
    }
    
    
    
}
