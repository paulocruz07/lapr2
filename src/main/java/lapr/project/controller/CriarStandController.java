/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.io.Serializable;
import java.util.ArrayList;
import lapr.project.model.CentroExposicoes;
import lapr.project.model.Exposicao;
import lapr.project.model.RegistoExposicoes;
import lapr.project.model.RegistoStand;
import lapr.project.model.Stand;


/**
 *
 * @author lapr grupo 3
 */
public class CriarStandController implements Serializable{
    
    private Stand stand;
    private RegistoStand rs;
    
     /**
     * Variavel de instancia CentroExposicoes ce
     */
    private CentroExposicoes ce;
    /**
     * Variavel de instancia Exposicao e
     */
    private Exposicao e;
    
    
    public CriarStandController(Stand stand, CentroExposicoes ce){
        this.stand=stand;
         this.ce=ce;
        this.e=ce.getRegistoExposicoes().getListaExposicoes().get(0);
    }
    
    /**
     * Retorna uma lista de Exposicoes
     * @return 
     */
    public ArrayList<Exposicao> getListaExposicoes() {
        RegistoExposicoes re= ce.getRegistoExposicoes();
        return re.getListaExposicoes();
    }
    /**
     * Retorna Exposicao
     * @return 
     */
    public Exposicao getExposicao(){
        return e;
    }
    /**
     * Atualiza Exposicao
     * @param exposicao 
     */
     public void setExposicao(Exposicao exposicao){
         this.e=exposicao;
     }
    
    public void novoStand(String nome, double area){
       stand.setNome(nome);
       stand.setArea(area);
        
    }
    
    public boolean registoStand(){
      boolean st = rs.RegistoStand(stand);
      return st;
        
    }

    
}
