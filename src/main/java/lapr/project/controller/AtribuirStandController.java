/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.io.Serializable;
import java.util.ArrayList;
import lapr.project.model.Candidatura;
import lapr.project.model.CentroExposicoes;
import lapr.project.model.Exposicao;
import lapr.project.model.ListaCandidaturas;
import lapr.project.model.Organizador;
import lapr.project.model.RegistoExposicoes;
import lapr.project.model.RegistoStand;
import lapr.project.model.Stand;

/**
 *
 * @author Jorge
 */
public class AtribuirStandController implements Serializable{
    
    private CentroExposicoes ce;
    private RegistoExposicoes re;
    private ListaCandidaturas rc;
    private RegistoStand rs; 
    private Exposicao exp;
    
        public AtribuirStandController(CentroExposicoes ce){
            this.ce = ce;
            this.re=ce.getRegistoExposicoes();
            this.rc = exp.getRegistoCandidaturas();
            this.rs = ce.getRegistoStands();
        }

    public void getListaExposicoes(Organizador o){
        ArrayList<Exposicao> lexp = new ArrayList();
        re = ce.getRegistoExposicoes();
        //Fazer estado
        re.getListaExposicoesDecididas(o);
    }
    
    public void setExposicao(Exposicao exp){
        this.exp = exp;
    }
    
    public void getCandidaturasAceites(){
        rc = exp.getRegistoCandidaturas();
        rc.getListaCandidaturasAceites();
    }
    
    public ArrayList<Stand> getListaStands(){
        rs = ce.getRegistoStands();
        return rs.getListaStands();
    }
    
    public void setStands(Stand s){
        exp.novaAtribuicao(exp,s);
    }
    
    public void registaDados(){
        exp.registaDados();
    }
}
