/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.io.Serializable;
import java.util.*;
import javax.swing.JOptionPane;
import lapr.project.model.*;

/**
 *
 * @author 1150855_1150856
 */

public class AtribuirCandidaturaController implements Serializable{
    
     /**
     * variável de instância CentroExposicoes ce.
     */
    private CentroExposicoes ce;
    
    /**
     * variável de instância Exposicao e.
    */ 
    private Exposicao e;
    
    /**
     * variável de instância MecanismoAtribuicao ma.
    */ 
    private MecanismoAtribuicao ma;
    /**
     * variável de instância do tipo ArrayList<Atribuicao> la.
    */ 
    private ArrayList<Atribuicao> la;
    /**
     * variável de instância ListaOrganizadores ls.
    */
    private ListaOrganizadores ls;
   /**
     * variável de instância ListaAtribuicoes lAtribuicao.
    */
    private ListaAtribuicoes lAtribuicao;
    
    /**
     * Contrutor com paremetro ce
     * @param ce - variavel do tipo CentroExposicoes passada por parametro
     */
    public AtribuirCandidaturaController(CentroExposicoes ce){
        this.ce = ce;
        JOptionPane.showMessageDialog(null, "olaaaa + "+ce.getRegistoExposicoes().getListaExposicoes());
        this.e=ce.getRegistoExposicoes().getListaExposicoes().get(0);
        this.ls=e.getListaOrganizadores();
        this.lAtribuicao=e.getRegistoAtribuicao();
    }
    
    /**
     * Retorna lista de exposicoes
     * @param o - variavel do tipo Organizador passada por parametro
     * @return 
     */
    public ArrayList<Exposicao> getListaExposicoes(Organizador o){
        RegistoExposicoes re= ce.getRegistoExposicoes();
        return re.getListaExposicoesDoOrganizadorNaoAtribuidas(o);
    }
    /**
     * Retorna lista de mecanismos
     * @return 
     */
    public ArrayList<String> getListaMecanismos(){
        RegistoMecanismos rm = ce.getRegistoMecanismos();
        
        return rm.getListaMecanismos();
    }
    /**
     * Retorna lista atribuicoes
     * @return 
     */
     public ArrayList<Atribuicao> getListaAtribuicoes(){
        return ma.getListaAtribuicoes(e);
    }
     /**
      * Retorna lista de organizador
      * @return 
      */
      public ArrayList<Organizador> getListaOrganizador(){
       ArrayList<Organizador>lorg=new ArrayList();
        ArrayList<Exposicao>le=ce.getRegistoExposicoes().getListaExposicoes();
        for(Exposicao e :le ){
            ArrayList<Organizador>lorgcopia=e.getListaOrganizadores().getListaOrganizadores();
            for(Organizador org:lorgcopia){
                lorg.add(org);
            }
        }
        return lorg;
    }

    /**
     * Atualiza centro de exposicoes, exposicao, lista de organizadores e lista de atribuicao
     * @param ce the ce to set
     */
    public void setCe(CentroExposicoes ce) {
        this.ce = ce;
        this.e=ce.getRegistoExposicoes().getListaExposicoes().get(0);
        this.ls=e.getListaOrganizadores();
        this.lAtribuicao=e.getRegistoAtribuicao();
        
    }

    /**
     * Atualiza mecanismos de atribuicao
     * @param ma the ma to set
     */
    public void setMa(MecanismoAtribuicao ma) {
        this.ma = ma;
    }

    /**
     * Atualiza lista de atribuicao
     * @param la the la to set
     */
    public void setLa(ArrayList<Atribuicao> la) {
        this.la = la;
    }
    
    /**
     * Atualiza Exposicao
     * @param e - variavel do tipo Exposicao passada por parametro
     */
    public void setExposicao(Exposicao e){
        this.e = e;
        this.ls=e.getListaOrganizadores();
        this.lAtribuicao=e.getRegistoAtribuicao();
    }
    
    /**
     * Atualiza çosta de organizadores
     * @param array 
     */
    public void setListaOrganizador(ListaOrganizadores array){
        ls.setListaOrganizadores(array.getListaOrganizadores());
    }

    /**
     * atualiza lista de organizadores
     * @param ls the ls to set
     */
    public void setLs(ListaOrganizadores ls) {
        this.ls = ls;
    }

    /**
     * atualiza lista de atribuicao
     * @param lAtribuicao the lAtribuicao to set
     */
    public void setlAtribuicao(ListaAtribuicoes lAtribuicao) {
        e.setListaAtribuicoes(lAtribuicao);
        this.lAtribuicao = lAtribuicao;
    }
    public void setExposicaoAtribuida(){
        e.setEstado(new ExposicaoEstadoCandidaturasAtribuidas(e));
    }
    
    /**
     * Regista atribuicoes e retorna true ou false
     * @return 
     */
    public boolean registarAtribuicoes(){
        return lAtribuicao.registarAtribuicoes(la);
    }
    
   
   
}
