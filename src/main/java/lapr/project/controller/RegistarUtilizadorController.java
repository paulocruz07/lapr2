/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.io.Serializable;
import java.util.ArrayList;
import lapr.project.model.CentroExposicoes;
import lapr.project.model.RegistoUtilizadores;
import lapr.project.model.Utilizador;

/**
 *
 * @author Ines Moura
 */
public class RegistarUtilizadorController implements Serializable{
    private Utilizador u = new Utilizador();
    private CentroExposicoes ce;
    private RegistoUtilizadores ru;
    
    public RegistarUtilizadorController(CentroExposicoes ce){
        this.ce = ce;  
    }

    /**
     * @return the ce
     */
    public CentroExposicoes getCe() {
        return ce;
    }
    
    public void novoUtilizador(){
        ru = getCe().getRegistoUtilizadores();
        u = ru.novoUtilizador();
    }
    
    
    public boolean setDadosEntrada(String nome, String email, String username, String password, String funcao){
        boolean b1 = u.setNome(nome);
        boolean b2 = u.setEmail(email);
        u.setUsername(username);
        boolean b3 = u.setPassword(password);
        u.setFuncao(funcao);
        u.setRegistado(false);
        
        if(b1==false || b2==false || b3==false)
            return false;
        
        return true;
    }
    
    public boolean registaUtilizador(){
        boolean b = ru.registaDados(u);
        return b;
    }
}
