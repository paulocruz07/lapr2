/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.io.Serializable;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import lapr.project.model.AlterarParaCandidaturasAbertas;
import lapr.project.model.AlterarParaCandidaturasFechadas;
import lapr.project.model.AlterarParaConflitosAlterados;
import lapr.project.model.CentroExposicoes;
import lapr.project.model.Exposicao;
import lapr.project.model.ExposicaoEstadoInicial;
import lapr.project.model.ListaOrganizadores;
import lapr.project.model.Local;
import lapr.project.model.Organizador;
import lapr.project.model.RegistoExposicoes;
import lapr.project.model.RegistoUtilizadores;
import lapr.project.model.Utilizador;

/**
 *
 * @author lapr grupo 3
 */
public class CriarExposicaoController implements Serializable{
    private final CentroExposicoes m_oCE;
    private Exposicao m_exposicao;
    private RegistoUtilizadores ru;
    
    public CriarExposicaoController(CentroExposicoes ce)
    {
        this.m_oCE = ce;
        
    }
     public void novaExposicao()
    {
        RegistoExposicoes re= m_oCE.getRegistoExposicoes();
        m_exposicao = re.novaExposicao();
        m_exposicao.setEstado(new ExposicaoEstadoInicial(m_exposicao));
    }
     
     public void setDados(String titulo,String descricao,String dataInicio,String dataFim,String subInicio,String subFim,String local,String dataLimiteConflitos){
         m_exposicao.setTitulo(titulo);
         m_exposicao.setDescricao(descricao);
         m_exposicao.setDataInicio(dataInicio);
         m_exposicao.setDataFim(dataFim);
         m_exposicao.setSubInicio(subInicio);
         m_exposicao.setSubFim(subFim);
         m_exposicao.setLocal(new Local(local));
         m_exposicao.setDataLimiteConflitos(dataLimiteConflitos);
     }
     
     public Utilizador getUtilizador(String userID){
         RegistoUtilizadores ru= m_oCE.getRegistoUtilizadores();
          return ru.getUtilizador(userID);
     } 
     
     public void addOrganizador(Utilizador u){
         ListaOrganizadores lo=m_exposicao.getListaOrganizadores();
         Organizador o = new Organizador(u);
         lo.addOrganizador(o);
     }
     
     public ArrayList<Utilizador> getListaUtilizadores() {
        ru = m_oCE.getRegistoUtilizadores();
        return ru.getListaUtilizadores();
    }
     
     public boolean validaExposicao(){
         return m_oCE.getRegistoExposicoes().validaExposicao1(m_exposicao);
     }
    public boolean registaExposicao(){
       try{
            m_oCE.getRegistoExposicoes().registaExposicao(m_exposicao);
            AlterarParaCandidaturasAbertas paraCandAbertas = new AlterarParaCandidaturasAbertas(m_exposicao);
            m_oCE.schedule(paraCandAbertas,m_exposicao.getSubInicio());
            AlterarParaCandidaturasFechadas paraCandFechadas = new AlterarParaCandidaturasFechadas(m_exposicao);
            m_oCE.schedule(paraCandFechadas,m_exposicao.getSubFim());
            DetetarConflitosController uc13Control = new DetetarConflitosController(m_exposicao);
            m_oCE.schedule(uc13Control,m_exposicao.getSubFim());
            AlterarParaConflitosAlterados paraConflAlterados = new AlterarParaConflitosAlterados();
            m_oCE.schedule(paraConflAlterados,m_exposicao.getDataLimiteConflitos());
            
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Erro na criação dos timers");
            return false;
        }
       
       return true;
    }
    
    public CentroExposicoes getCe() {
        return m_oCE;
    }
}
