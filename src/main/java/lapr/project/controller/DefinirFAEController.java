/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.io.Serializable;
import java.util.ArrayList;
import javax.swing.JOptionPane;

import lapr.project.model.CentroExposicoes;
import lapr.project.model.Exposicao;
import lapr.project.model.FAE;
import lapr.project.model.ListaFAE;
import lapr.project.model.Organizador;
import lapr.project.model.RegistoExposicoes;
import lapr.project.model.RegistoUtilizadores;
import lapr.project.model.Utilizador;

/**
 *
 * @author Jorge
 */
public class DefinirFAEController implements Serializable{

    private CentroExposicoes ce;
    private RegistoExposicoes re;
    private RegistoUtilizadores ru;
    private Exposicao exp;
    private ListaFAE lfae = new ListaFAE();

    public DefinirFAEController(CentroExposicoes centroexposicoes) {
        ce = centroexposicoes;
        this.re = ce.getRegistoExposicoes();
        this.ru = ce.getRegistoUtilizadores();
    }

    /**
     * @return the exp
     */
    public Exposicao getExp() {
        return exp;
    }
    
    public ListaFAE getFAE(){
        return lfae;
    }
    

    public ArrayList<Exposicao> getExposicoesOrganizador(Organizador o) {
        ArrayList<Exposicao> lSemFAE = new ArrayList();
        lSemFAE = re.getExposicoesSemFAE(o);
        return lSemFAE;
    }

    public ArrayList<Utilizador> getListaUtilizadores() {
        ru = ce.getRegistoUtilizadores();
        return ru.getListaUtilizadores();
    }
    

    public void setExposicao(Exposicao e) {
        this.exp = e;
        lfae=getExp().getListaFAE();
    }
    
    public boolean registaMembroFAE(FAE f) {
        return lfae.registarFAE(f);
    }

    public FAE addFAE(Utilizador u, Exposicao e) {
        lfae = e.getListaFAE();
        FAE fae=lfae.addCriaFAE(u, e);
        return fae;
    }

    public void atualizaExposicao() {
        getExp().setFaeDefinido();
    }
}
