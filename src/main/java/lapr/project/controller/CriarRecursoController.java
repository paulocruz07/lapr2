/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.io.Serializable;
import lapr.project.model.Recurso;
import lapr.project.model.RegistoRecurso;

/**
 *
 * @author lapr grupo 3
 */
public class CriarRecursoController implements Serializable{
    private Recurso recurso;
    private RegistoRecurso rr;
    
    
    
    public void novoRecurso(String descricao){
        recurso.setDescricao(descricao);  
    }
    
    
    
    public boolean registaRecurso(){
      boolean st = rr.registaRecurso(recurso);
      return st;
    }
    
}
